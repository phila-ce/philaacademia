<?php

namespace Database\Factories;

use App\Models\fidelGifted;
use Illuminate\Database\Eloquent\Factories\Factory;

class FidelGiftedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = fidelGifted::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
