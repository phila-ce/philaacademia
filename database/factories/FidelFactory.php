<?php

namespace Database\Factories;

use App\Models\fidel;
use Illuminate\Database\Eloquent\Factories\Factory;

class FidelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = fidel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->name,
            'prenom' => $this->faker->lastName,
            'sexe' => $this->faker->randomElement(["Homme", "Femme"]),
            'etatCivil' => $this->faker->randomElement(["Célibataire", "Marié"]),
            'baptiser' => $this->faker->randomElement([1, 0]),
            'commune' => $this->faker->randomElement(["Limeté", "Gombe", "Ngaliema", "Kisenso"]),
            'est_de' => $this->faker->randomElement(['Phila','Exterieur']),
            'lieu' =>"Kinshasa",
            'datenaissance' => $this->faker->date,
            'phone' => $this->faker->phonenumber,
            'email' => $this->faker->unique()->safeEmail(),
            'user_id' => 1, 
        ];
    }
}
