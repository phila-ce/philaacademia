<?php

namespace Database\Factories;

use App\Models\examensCours;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExamensCoursFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = examensCours::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
