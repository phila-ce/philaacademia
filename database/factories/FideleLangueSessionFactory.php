<?php

namespace Database\Factories;

use App\Models\fideleLangueSession;
use Illuminate\Database\Eloquent\Factories\Factory;

class FideleLangueSessionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = fideleLangueSession::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
