<?php

namespace Database\Factories;

use App\Models\cultiveFidel;
use Illuminate\Database\Eloquent\Factories\Factory;

class CultiveFidelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = cultiveFidel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
