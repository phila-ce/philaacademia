<?php

namespace Database\Factories;

use App\Models\cursusFidel;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursusFidelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = cursusFidel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
