<?php

namespace Database\Factories;

use App\Models\tpCours;
use Illuminate\Database\Eloquent\Factories\Factory;

class TpCoursFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = tpCours::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
