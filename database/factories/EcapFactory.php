<?php

namespace Database\Factories;

use App\Models\ecap;
use Illuminate\Database\Eloquent\Factories\Factory;

class EcapFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ecap::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
