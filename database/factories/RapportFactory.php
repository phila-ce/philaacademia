<?php

namespace Database\Factories;

use App\Models\rapport;
use Illuminate\Database\Eloquent\Factories\Factory;

class RapportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = rapport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
