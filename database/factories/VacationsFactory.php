<?php

namespace Database\Factories;

use App\Models\vacations;
use Illuminate\Database\Eloquent\Factories\Factory;

class VacationsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = vacations::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
