<?php

namespace Database\Factories;

use App\Models\smallgroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class SmallgroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = smallgroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
