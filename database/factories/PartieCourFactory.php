<?php

namespace Database\Factories;

use App\Models\partieCour;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartieCourFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = partieCour::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
