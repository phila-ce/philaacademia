<?php

namespace Database\Factories;

use App\Models\travail;
use Illuminate\Database\Eloquent\Factories\Factory;

class TravailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = travail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
