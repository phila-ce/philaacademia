<?php

namespace Database\Factories;

use App\Models\reponseExament;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReponseExamentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = reponseExament::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
