<?php

namespace Database\Factories;

use App\Models\cursus;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = cursus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
