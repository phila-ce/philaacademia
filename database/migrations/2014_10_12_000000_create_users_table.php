<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('prenom')->nullable();
            $table->string('sexe')->nullable();
            $table->date('datenaissace')->nullable();
            $table->string('telephone')->nullable();
            $table->string('photo')->nullable();
            $table->string('commune')->nullable();
            $table->string('quartier')->nullable();
            $table->string('avenue')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('etatCivil')->nullable();
            $table->string('ministere')->nullable();
            $table->string('profession')->nullable();
            $table->string('niveauEtude')->nullable();
            $table->string('filiere')->nullable();
            $table->enum('niveau', array('1', '2','3', '4'))->default('1');
            $table->enum('mentor', array('0', '1'))->default('0');
            $table->enum('ecap', array('0', '1'))->default('0');
            $table->enum('role', array('ouvrier', 'enseignant','assistant','admin'))->default('ouvrier');
            $table->boolean('disponible')->default(1);
            $table->boolean('visible')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
