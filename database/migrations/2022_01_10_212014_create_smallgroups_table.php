<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\metamorpho;
use App\Models\fidel;
use App\Models\User;

class CreateSmallgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smallgroups', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(metamorpho::class);
            $table->string('nom')->nullable();
            $table->string('quota')->nullable();
            $table->enum('etat', array('actif', 'cloturer'))->default('actif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smallgroups');
    }
}
