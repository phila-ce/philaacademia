<?php

use App\Models\ecap;
use App\Models\fidel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFidelEcapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidel_ecaps', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(ecap::class);
            $table->foreignIdFor(fidel::class);
            $table->string('etat')->default('0');
            $table->text('observation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidel_ecaps');
    }
}
