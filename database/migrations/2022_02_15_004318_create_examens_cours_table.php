<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\chapitre;

class CreateExamensCoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        Schema::create('examens_cours', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(chapitre::class);   
            $table->string('question')->nullable();
            $table->string('instruction')->nullable();
            $table->times('temps')->nullable();
            $table->int('ponderation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examens_cours');
    }
}
