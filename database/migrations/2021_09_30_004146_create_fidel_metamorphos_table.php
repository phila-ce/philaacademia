<?php

use App\Models\User;
use App\Models\fidel;
use App\Models\mentor;
use App\Models\metamorpho;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFidelMetamorphosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidel_metamorphos', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(metamorpho::class);
            $table->foreignIdFor(fidel::class);
            $table->foreignIdFor(User::class);
            $table->string('etat')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidel_metamorphos');
    }
}
