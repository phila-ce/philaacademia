<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\smallgroup;

class CreateFidelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidels', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prenom');
            $table->string('sexe');
            $table->string('datenaissance')->nullable();
            $table->string('lieu')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('etatCivil')->nullable();
            $table->boolean('baptiser')->default(0);
            $table->string('commune')->nullable();
            $table->string('quartier')->nullable();
            $table->string('avenue')->nullable();
            $table->boolean('visible')->default(1);
            $table->foreignIdFor(User::class)->nullable();
            $table->foreignIdFor(smallgroup::class)->nullable();
            $table->enum('etat_activite', array('enregistrer', 'active','suspendu', 'fini'))->default('enregistrer');
            $table->enum('est_de', array('Phila', 'Exterieur'))->default('Phila');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidels');
    }
}
