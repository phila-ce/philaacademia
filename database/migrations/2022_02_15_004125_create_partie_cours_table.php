<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\chapitre;

class CreatePartieCoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partie_cours', function (Blueprint $table) {
            $table->id();            
            $table->foreignIdFor(chapitre::class);   
            $table->string('titrePartie')->nullable();
            $table->string('nombreHeure')->nullable();
            $table->string('video')->nullable();
            $table->string('ordre')->nullable();
            $table->text('description')->nullable();
            $table->enum('tp', array('oui', 'non'))->default('non');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partie_cours');
    }
}
