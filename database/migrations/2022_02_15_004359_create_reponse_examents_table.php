<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\examensCours;
use App\Models\fidel;

class CreateReponseExamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reponse_examents', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(examensCours::class); 
            $table->text('reponse')->nullable();
            $table->boolean('type');
            $table->string('commentaire')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reponse_examents');
    }
}
