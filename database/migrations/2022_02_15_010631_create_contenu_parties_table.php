<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\partieCour;

class CreateContenuPartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contenu_parties', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(partieCour::class);   
            $table->string('nombreHeure')->nullable();
            $table->string('pdf')->nullable();
            $table->text('ecrit')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contenu_parties');
    }
}
