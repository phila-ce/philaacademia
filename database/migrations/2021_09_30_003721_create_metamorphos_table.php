<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetamorphosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metamorphos', function (Blueprint $table) {
            $table->id();
            $table->string('session');
            $table->date('debut');
            $table->date('fin');
            $table->text('description');
            $table->enum('etat', array('active','suspendu', 'cloturer'))->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metamorphos');
    }
}
