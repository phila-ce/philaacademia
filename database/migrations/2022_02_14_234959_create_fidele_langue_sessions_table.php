<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ecapLangue;
use App\Models\fidel;

class CreateFideleLangueSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidele_langue_sessions', function (Blueprint $table) {
            $table->id();            
            $table->foreignIdFor(ecapLangue::class);
            $table->foreignIdFor(fidel::class);
            $table->text('description')->nullable();
            $table->text('etat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidele_langue_sessions');
    }
}
