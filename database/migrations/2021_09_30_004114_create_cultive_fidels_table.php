<?php

use App\Models\cultive;
use App\Models\fidel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCultiveFidelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cultive_fidels', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(cultive::class);
            $table->foreignIdFor(fidel::class);
            $table->string('etat')->default('0');
            $table->text('observation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cultive_fidels');
    }
}
