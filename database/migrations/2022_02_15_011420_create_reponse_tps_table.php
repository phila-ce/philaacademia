<?php

use App\Models\fidel;
use App\Models\tpCours;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReponseTpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reponse_tps', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(tpCours::class);   
            $table->foreignIdFor(fidel::class);   
            $table->string('commentaire')->nullable();
            $table->string('fichier')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reponse_tps');
    }
}
