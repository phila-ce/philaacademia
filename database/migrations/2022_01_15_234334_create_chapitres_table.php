<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ecapLangue;
use App\Models\User;
use App\Models\cour;

class CreateChapitresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapitres', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(cour::class);
            $table->foreignIdFor(ecapLangue::class);
            $table->foreignIdFor(User::class);
            $table->string('titre')->nullable();
            $table->text('apercu')->nullable();
            $table->string('cover')->nullable();
            $table->text('prerequis')->nullable();
            $table->text('description')->nullable();
            $table->string('videoPresentation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapitres');
    }
}
