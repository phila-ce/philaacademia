<?php

namespace App\Observers;

use App\Models\cultiveFidel;
use App\Http\Controllers\FidelController;
use App\Models\fidel;

class cvcObserver
{
    /**
     * Handle the cultiveFidel "created" event.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return void
     */
    public function created(cultiveFidel $cultiveFidel)
    {
        $fidel=fidel::find($cultiveFidel->fidel_id);
        $mail=new FidelController();
        if ($fidel->email!='' ) {
           $mail->mail($fidel->email,$fidel,'Debut Cultivé la vie de christ');
        } 
    }

    /**
     * Handle the cultiveFidel "updated" event.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return void
     */
    public function updated(cultiveFidel $cultiveFidel)
    {
        $fidel=fidel::find($cultiveFidel->fidel_id);
        $mail=new FidelController();
        if ($cultiveFidel->wasChanged('etat')) {
            $mail->mail($fidel->email,$fidel,'Cloture cultivé la vie de christ');
        } else{
            $mail->mail($fidel->email,$fidel,'Debut Cultivé la vie de christ');
        }
    }

    /**
     * Handle the cultiveFidel "deleted" event.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return void
     */
    public function deleted(cultiveFidel $cultiveFidel)
    {
        //
    }

    /**
     * Handle the cultiveFidel "restored" event.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return void
     */
    public function restored(cultiveFidel $cultiveFidel)
    {
        //
    }

    /**
     * Handle the cultiveFidel "force deleted" event.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return void
     */
    public function forceDeleted(cultiveFidel $cultiveFidel)
    {
        //
    }
}
