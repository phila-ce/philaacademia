<?php

namespace App\Observers;

use App\Models\fidel;
use App\Models\fidelMetamorpho;
use App\Http\Controllers\FidelController;

class metafidelObserver
{
    /**
     * Handle the fidelMetamorpho "created" event.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return void
     */
    public function created(fidelMetamorpho $fidelMetamorpho)
    {
        $fidel=fidel::find($fidelMetamorpho->fidel_id);
        $mail=new FidelController();
        if ($fidel->email!='' ) {
            $mail->mail($fidel->email,$fidel,'Debut Metamorpho');
        } 
    }

    /**
     * Handle the fidelMetamorpho "updated" event.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return void
     */
    public function updated(fidelMetamorpho $fidelMetamorpho)
    {
        $fidel=fidel::find($fidelMetamorpho->fidel_id);
        $mail=new FidelController();
        if ($fidel->email!='' ) {
            $mail->mail($fidel->email,$fidel,'Debut Metamorpho');
        } 
    }

    /**
     * Handle the fidelMetamorpho "deleted" event.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return void
     */
    public function deleted(fidelMetamorpho $fidelMetamorpho)
    {
        //
    }

    /**
     * Handle the fidelMetamorpho "restored" event.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return void
     */
    public function restored(fidelMetamorpho $fidelMetamorpho)
    {
        //
    }

    /**
     * Handle the fidelMetamorpho "force deleted" event.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return void
     */
    public function forceDeleted(fidelMetamorpho $fidelMetamorpho)
    {
        //
    }
}
