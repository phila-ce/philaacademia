<?php

namespace App\Observers;

use App\Models\fidel;
use App\Http\Controllers\FidelController;

class fidelObserver
{
    /**
     * Handle the fidel "created" event.
     *
     * @param  \App\Models\fidel  $fidel
     * @return void
     */
    public function created(fidel $fidel)
    {
        
        $mail=new FidelController();
        if ($fidel->email!='' ) {
            $mail->mail($fidel->email,$fidel,'Inscription PHILA ACADEMIA');
        } 
    }

    /**
     * Handle the fidel "updated" event.
     *
     * @param  \App\Models\fidel  $fidel
     * @return void
     */
    public function updated(fidel $fidel)
    {
        $mail=new FidelController();
        if($fidel->wasChanged('smallgroup_id')){
            if ($fidel->smallgroup_id==null) {
               $mail->mail($fidel->email,$fidel,'Vous êtes detaché du smallgroup pour un autre!');
            } else {
                $mail->mail($fidel->email,$fidel,'Information de votre small group');
            }
           
        }
    }

    /**
     * Handle the fidel "deleted" event.
     *
     * @param  \App\Models\fidel  $fidel
     * @return void
     */
    public function deleted(fidel $fidel)
    {
        //
    }

    /**
     * Handle the fidel "restored" event.
     *
     * @param  \App\Models\fidel  $fidel
     * @return void
     */
    public function restored(fidel $fidel)
    {
        //
    }

    /**
     * Handle the fidel "force deleted" event.
     *
     * @param  \App\Models\fidel  $fidel
     * @return void
     */
    public function forceDeleted(fidel $fidel)
    {
        //
    }
}
