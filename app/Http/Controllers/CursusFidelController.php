<?php

namespace App\Http\Controllers;

use App\Models\cursusFidel;
use Illuminate\Http\Request;

class CursusFidelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cursusFidel  $cursusFidel
     * @return \Illuminate\Http\Response
     */
    public function show(cursusFidel $cursusFidel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cursusFidel  $cursusFidel
     * @return \Illuminate\Http\Response
     */
    public function edit(cursusFidel $cursusFidel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cursusFidel  $cursusFidel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cursusFidel $cursusFidel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cursusFidel  $cursusFidel
     * @return \Illuminate\Http\Response
     */
    public function destroy(cursusFidel $cursusFidel)
    {
        //
    }
}
