<?php

namespace App\Http\Controllers;

use App\Models\fidel;
use App\Models\cultive;
use App\Models\metamorpho;
use App\Models\cultiveFidel;
use Illuminate\Http\Request;
use App\Models\fidelMetamorpho;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CultiveController extends Controller
{
   public $fid;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.cvc.cultive');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fidels=fidel::where([['visible','=','1'],['etat_activite','=','enregistrer'],['est_de','=','Phila']])->get();
        $sessionActive=cultive::where('etat','active')->whereNotNull('session')->get();
        $title='Début-CVC';
        // $sessionActive = $collection->whereNotNull('session');
        //  dd($fidels);
        return view('pages.cvc.g_cultive',compact('fidels','sessionActive','title'));
    }
    public function debutMeta()
    {
        $fidels=fidel::selectRaw('fidels.*')
        ->join('cultive_fidels','cultive_fidels.fidel_id','fidels.id')
        ->where([['visible','=','1'],['etat_activite','=','active'],['cultive_fidels.etat','=','0'],['est_de','=','Phila']])
        ->get();
        $sessionActive=metamorpho::where('etat','active')->whereNotNull('session')->get();
        $title='Début-metamorpho';
        return view('pages.cvc.deb_meta',compact('fidels','sessionActive','title'));
    }
    public function gestioncvc()
    {
        $fidels=fidel::where([['visible','=','1'],['etat_activite','=','active'],['est_de','=','Phila']])->get();
        $fidelsall=fidel::selectRaw('fidels.*,cultive_fidels.etat')
        ->join('cultive_fidels','cultive_fidels.fidel_id','fidels.id')
        ->where([['fidels.visible','=','1'],['fidels.est_de','=','Phila']])
        ->get();
      //  $fidelsEncour=cultiveFidel::where('etat','0')->get();
      //  $fidelsFini=cultiveFidel::where('etat','1')->get();
        $sessions=cultive::all();
        $title='Gestion-Cultivé';
        $fidelsEncour= fidel::selectRaw('fidels.*,cultive_fidels.etat')
        ->join('cultive_fidels','cultive_fidels.fidel_id','fidels.id')
        ->where([['fidels.visible','=','1'],['cultive_fidels.etat','0'],['fidels.est_de','=','Phila']])
        ->get();
        $fidelsFini= fidel::selectRaw('fidels.*,cultive_fidels.etat')
        ->join('cultive_fidels','cultive_fidels.fidel_id','fidels.id')
        ->where([['fidels.visible','=','1'],['cultive_fidels.etat','1'],['fidels.est_de','=','Phila']])
        ->get();
    //  dd($f);
        // $sessionActive = $collection->whereNotNull('session');
       // dd($fidels[0]->cultives[0]->pivot->fidel_id);
        return view('pages.cvc.g_cvc',compact('fidels','sessions','title','fidelsall','fidelsEncour','fidelsFini'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function debutCvc(Request $request)
    {
        $rep=false;
        $active=cultive::where('etat','active')->first();
        if($active){
          $fidels=explode(',',$request->tabFidel);
          for($i=0;$i<count($fidels);$i++){
            $rep=cultiveFidel::updateOrCreate([
                'cultive_id'=>$active->id,
                'fidel_id'=>$fidels[$i],
              ]);
              $rap =fidel::findOrFail($fidels[$i]);
                $rap->update([
                    'etat_activite' => 'active'
                ]);
          }

          if ($rep) {
            return response()->json(['reponse' => true,'msg' => count($fidels).' Fidèle(s) enregistrer pour la session '.$active->session.' de CVC ']);
          } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur d\'inscription à CVC']);
          }

        }else{
             return response()->json(['reponse' => false,'msg' => 'Aucune session ouverte pour le moment!']);
           }
    }
    public function debutMetaAffectation(Request $request)
    {
        $fid=new FidelController();
        $rep=false;
        $active=metamorpho::where('etat','active')->first();
        if($active){
          $fidels=explode(',',$request->tabFidel);
          for($i=0;$i<count($fidels);$i++){
            $rep=fidelMetamorpho::updateOrCreate([
                'metamorpho_id'=>$active->id,
                'fidel_id'=>$fidels[$i],
                'user_id'=>Auth::user()->id,
              ]);
              $rap =cultiveFidel::where('fidel_id',$fidels[$i])->first();
                $rap->update([
                    'etat' => '1'
                ]);
          }

          if ($rep) {
            return response()->json(['reponse' => true,'msg' => count($fidels).' Fidèle(s) enregistrer pour la session '.$active->session.' de Metamorpho, un mail de notification sera envoyer']);
          } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur d\'inscription à Metamorpho']);
          }

        }else{
             return response()->json(['reponse' => false,'msg' => 'Aucune session de metamorpho n\'est ouverte pour le moment!']);
           }
    }
    public function store(Request $request)
    {

       $ok= Validator::make($request->all(),[
        'session'=>'unique:cultives',
        'description'=>'required',
            ]);
        if(!$ok->fails()){
            $active=cultive::where('etat','active')->first();
            if ($active) {
                  $session=cultive::firstOrCreate(
                    [
                        'session'=>$request->session,
                        'debut'=>$request->debut,
                        'fin'=>$request->fin,
                        'etat'=>'suspendu',
                    ],
                    [
                        'description'=>$request->description,
                    ]
                );
                if ($session) {
                    return response()->json(['reponse' => true,'msg' => 'la session '.$request->session.' est crée mais est en suspend car une autre est en cours!']);
                } else {
                    return response()->json(['reponse' => false,'msg' => 'Erreur d\'enregistrement de la session!']);
                }
            } else {
                $session=cultive::firstOrCreate(
                    [
                        'session'=>$request->session,
                        'debut'=>$request->debut,
                        'fin'=>$request->fin,
                    ],
                    [
                        'description'=>$request->description,
                    ]
                );
                if ($session) {
                    return response()->json(['reponse' => true,'msg' => 'la session '.$request->session.' est crée avec succès!']);
                } else {
                    return response()->json(['reponse' => false,'msg' => 'Erreur d\'enregistrement de la session!']);
                }
            }
}else{
return response()->json(['reponse' => false,'msg' => 'Une session avec le nom de '.$request->session.' existe déjà, impossible de réecrer']);
}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cultive  $cultive
     * @return \Illuminate\Http\Response
     */
    public function show(cultive $cultive)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cultive  $cultive
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active=cultive::where('etat','active')->first();
        if (!$active) {
            $ouv=cultive::where('id',$id)->first();
          $ouvsession= $ouv->update([
                'etat' => 'active',
            ]);
            if($ouvsession){
                return response()->json(['reponse' => true,'msg','La session '.$ouv->session.' est ouvert']);
            }else{
                return response()->json(['reponse' => false,'msg','Erreur du modification!']);
            }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Une seesion est active, impossible d\'en a 2 simultanement!']);

        }
    }
    public function cloturer($id)
    {
        $active=cultive::where('id',$id)->first();
        if ($active) {
            $closession= $active->update([
                'etat' => 'cloturer',
            ]);
            if($closession){
                return response()->json(['reponse' => true,'msg','La session '.$active->session.' est cloturer']);
            }else{
                return response()->json(['reponse' => false,'msg','Erreur du modification!']);
            }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Aucune session trouvée!!']);

        }
    }
    public function suspendre($id)
    {
        $active=cultive::where('id',$id)->first();
        if ($active) {
            $closession= $active->update([
                'etat' => 'suspendu',
            ]);
            if($closession){
                return response()->json(['reponse' => true,'msg','La session '.$active->session.' est suspendu']);
            }else{
                return response()->json(['reponse' => false,'msg','Erreur du modification!']);
            }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Aucune session trouvée!!']);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cultive  $cultive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cultive $cultive)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cultive  $cultive
     * @return \Illuminate\Http\Response
     */
    public function destroy(cultive $cultive)
    {
        //
    }
}
