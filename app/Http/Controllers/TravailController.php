<?php

namespace App\Http\Controllers;

use App\Models\travail;
use Illuminate\Http\Request;

class TravailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\travail  $travail
     * @return \Illuminate\Http\Response
     */
    public function show(travail $travail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\travail  $travail
     * @return \Illuminate\Http\Response
     */
    public function edit(travail $travail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\travail  $travail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, travail $travail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\travail  $travail
     * @return \Illuminate\Http\Response
     */
    public function destroy(travail $travail)
    {
        //
    }
}
