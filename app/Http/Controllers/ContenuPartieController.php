<?php

namespace App\Http\Controllers;

use App\Models\contenuPartie;
use Illuminate\Http\Request;

class ContenuPartieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\contenuPartie  $contenuPartie
     * @return \Illuminate\Http\Response
     */
    public function show(contenuPartie $contenuPartie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\contenuPartie  $contenuPartie
     * @return \Illuminate\Http\Response
     */
    public function edit(contenuPartie $contenuPartie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\contenuPartie  $contenuPartie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, contenuPartie $contenuPartie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\contenuPartie  $contenuPartie
     * @return \Illuminate\Http\Response
     */
    public function destroy(contenuPartie $contenuPartie)
    {
        //
    }
}
