<?php

namespace App\Http\Controllers;

use App\Models\fidelGifted;
use Illuminate\Http\Request;

class FidelGiftedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fidelGifted  $fidelGifted
     * @return \Illuminate\Http\Response
     */
    public function show(fidelGifted $fidelGifted)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fidelGifted  $fidelGifted
     * @return \Illuminate\Http\Response
     */
    public function edit(fidelGifted $fidelGifted)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\fidelGifted  $fidelGifted
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fidelGifted $fidelGifted)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fidelGifted  $fidelGifted
     * @return \Illuminate\Http\Response
     */
    public function destroy(fidelGifted $fidelGifted)
    {
        //
    }
}
