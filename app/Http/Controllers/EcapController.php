<?php

namespace App\Http\Controllers;

use App\Models\ecap;
use Illuminate\Http\Request;

class EcapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pages.ecap..ecap');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ecap  $ecap
     * @return \Illuminate\Http\Response
     */
    public function show(ecap $ecap)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ecap  $ecap
     * @return \Illuminate\Http\Response
     */
    public function edit(ecap $ecap)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ecap  $ecap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ecap $ecap)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ecap  $ecap
     * @return \Illuminate\Http\Response
     */
    public function destroy(ecap $ecap)
    {
        //
    }
}
