<?php

namespace App\Http\Controllers;

use App\Models\fidelMetamorpho;
use Illuminate\Http\Request;

class FidelMetamorphoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return \Illuminate\Http\Response
     */
    public function show(fidelMetamorpho $fidelMetamorpho)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return \Illuminate\Http\Response
     */
    public function edit(fidelMetamorpho $fidelMetamorpho)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fidelMetamorpho $fidelMetamorpho)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fidelMetamorpho  $fidelMetamorpho
     * @return \Illuminate\Http\Response
     */
    public function destroy(fidelMetamorpho $fidelMetamorpho)
    {
        //
    }
}
