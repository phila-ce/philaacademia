<?php

namespace App\Http\Controllers;

use App\Models\cultiveFidel;
use Illuminate\Http\Request;

class CultiveFidelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return \Illuminate\Http\Response
     */
    public function show(cultiveFidel $cultiveFidel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return \Illuminate\Http\Response
     */
    public function edit(cultiveFidel $cultiveFidel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cultiveFidel $cultiveFidel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cultiveFidel  $cultiveFidel
     * @return \Illuminate\Http\Response
     */
    public function destroy(cultiveFidel $cultiveFidel)
    {
        //
    }
}
