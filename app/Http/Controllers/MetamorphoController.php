<?php
//
namespace App\Http\Controllers;

use com;
use App\Models\User;
use App\Models\fidel;
use App\Models\metamorpho;
use App\Models\smallgroup;
use Illuminate\Http\Request;
use App\Models\fidelMetamorpho;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FidelController;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\If_;

//la classe qui traite les mentors
class MetamorphoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $session=metamorpho::all();
        return view('pages.meta.metamorpho',compact('session'));
    }
    public function addmentor()
    {
        return view('pages..metaaddMentor');
    }
    public function gMentor()
    {
        $allMentor=User::where([['visible','=','1'],['mentor','=','1']])->get();
        $MentorDispo=User::where([['visible','=','1'],['mentor','=','1'],['disponible','=','1']])->get();
        $MentorEncour=User::selectRaw('Users.*,smallgroups.etat')
        ->join('smallgroups','smallgroups.user_id','Users.id')
        ->where([['visible','=','1'],['mentor','=','1'],['disponible','=','1']])->distinct()->get();
        //dd( $MentorEncour);
        return view('pages.meta.g_mentor',compact('allMentor','MentorDispo','MentorEncour'));
    }
    public function addSmallgroup()
    {
        $MentorDispo=User::where([['visible','=','1'],['mentor','=','1'],['disponible','=','1']])
        ->whereNotExists(function($query)
                {
                    $query->select('smallgroups.quota')
                          ->from('smallgroups')
                          ->whereRaw('users.id = smallgroups.user_id');
                })
        ->get();
        $smallFidel=smallgroup::with(['fidel','metamorpho','user'])->orderBy('nom')->get();
        return view('pages.meta.addSmallgroup',compact('MentorDispo','smallFidel'));
    }
    public function createSmllgroup(Request $request)
    {
        $session=metamorpho::where('etat','active')->first();
        if($session){
            $nomExist=smallgroup::where([['nom',$request->nom],['metamorpho_id',$session->id]])->first();
            $mentorExist=smallgroup::where([['user_id',$request->user_id],['metamorpho_id',$session->id]])->first();
            if($nomExist){
                return response()->json(['reponse' => false,'msg' => 'Impossible car un smallgroup portant ce nom existe déjà!']);
            }elseif($mentorExist){
                return response()->json(['reponse' => false,'msg' => 'Impossible, car ce mentor gère déjà un smallgroup!']);
            }
              $rep =smallgroup::create([
                    'user_id' => $request->user_id,
                    'nom' => $request->nom,
                    'quota' => $request->quota,
                    'metamorpho_id' => $session->id,
                ]);
            
          if ($rep) {
            return response()->json(['reponse' => true,'msg' =>'Small group crée avec succès']);
          } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur de creation']);
          }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Aucune session de metamorpho n\est ouvert, impossible de crée un small group!']);
        }
    }
    public function gestionSmallgroup()
    {
        $fidels=fidel::selectRaw('fidels.*')
        ->join('cultive_fidels','cultive_fidels.fidel_id','fidels.id')
        ->where([['visible','=','1'],['cultive_fidels.etat','=','1'],['fidels.est_de','=','Phila'],['smallgroup_id','=',null]])->orderBy('fidels.datenaissance')->get();
        
        $smlg=smallgroup::selectRaw('smallgroups.id as i,smallgroups.user_id,smallgroups.metamorpho_id,smallgroups.nom,smallgroups.quota,users.*')
        ->join('users','users.id','smallgroups.user_id')
        ->where('users.visible','=','1')->orderBy('users.datenaissace')->get();
        // $smlg=smallgroup::with('user')
        // ->where('users.visible','=','1')->orderBy('datenaissace')->get();
        
        return view('pages.meta.affectSmallgroup',compact('fidels','smlg'));
    }
    public function gRapport()
    {
        return view('pages.meta.gRapport');
    }
    public function envoyerEcap()
    {
        return view('pages.meta.affectEcap');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ok= Validator::make($request->all(),[
         'session'=>'unique:metamorphos',
         'description'=>'required',
             ]);
         if(!$ok->fails()){
             $active=metamorpho::where('etat','active')->first();
             if ($active) {
                // return response()->json(['reponse' => false,'msg' => 'Impossible de crée une session pendant que la précédente est active!']);
                 $session=metamorpho::firstOrCreate(
                     [
                         'session'=>$request->session,
                         'debut'=>$request->debut,
                         'fin'=>$request->fin,        
                         'etat'=>'suspendu',
                     ],
                     [
                         'description'=>$request->description,
                     ]
                 );
                 if ($session) {
                     return response()->json(['reponse' => true,'msg' => 'la session '.$request->session.' est crée mais est en suspend car une autre est en cours!']);
                 } else {
                     return response()->json(['reponse' => false,'msg' => 'Erreur d\'enregistrement de la session!']);
                 }
             } else {
                 $session=metamorpho::firstOrCreate(
                     [
                         'session'=>$request->session,
                         'debut'=>$request->debut,
                         'fin'=>$request->fin,
                     ],
                     [
                         'description'=>$request->description,
                     ]
                 );
                 if ($session) {
                     return response()->json(['reponse' => true,'msg' => 'la session '.$request->session.' est crée avec succès!']);
                 } else {
                     return response()->json(['reponse' => false,'msg' => 'Erreur d\'enregistrement de la session!']);
                 }
             }
            }else{
            return response()->json(['reponse' => false,'msg' => 'Une session avec le nom de '.$request->session.' existe déjà, impossible de réecrer']);
            }
     }

     public function affectationSmalg(Request $request)
    {

    //   dd($request->checksendmail);
        $fid=new FidelController();
        $rep=false;
        $active=metamorpho::where('etat','active')->first();
        if($active){
          $quota=smallgroup::find($request->smlgroup);
            $mentor=User::where('id',$quota->user_id)->first();
            $nbr=fidel::where('smallgroup_id',$request->smlgroup)->get();
            $fidels=explode(',',$request->tabFidel);
            $reste=(int)$quota->quota-$nbr->count();
            if (count($fidels)>(int)$quota->quota ) {
                return response()->json(['reponse' => false,'msg' => "Impossible, le nombre des mentores à affecter ne doit pas depassé le quota du smallgroup!"]);
            } else {
                if($reste==0){
                    return response()->json(['reponse' => false,'msg' => "Impossible, ce smallgroup a déjà atteind son quota!"]);
                    
                }elseif (count($fidels)>$reste) {
                        return response()->json(['reponse' => false,'msg' => "Impossible, le nombre des mentores à affecter ne doit pas être superieur au nombre restant qui est (".$reste.") pour atteindre le quota "]);
                    }
                     else {
                        for($i=0;$i<count($fidels);$i++){
                                    $rep= fidel::findOrFail($fidels[$i]);
                                    $rep->update([
                                        'smallgroup_id'=>$request->smlgroup,
                                    ]);                              
                                //  if ($request->checksendmail=='on') {
                                //  $fid->mail($rep->email,$rep,'Metamorpho, Affectation smallgroup');
                                //   }
                                }
                                
                                if ($rep) {
                                    // if ($request->checksendmail=='on') {
                                        return response()->json(['reponse' => true,'msg' => count($fidels).' Fidèle(s) affecté dans le smallgroup '.$quota->nom.'
                                        de Metamorpho avec comme mentor '.$mentor->name.'-'.$mentor->prenom.' et recevrons un mail de notification']);    
                                    // }
                                    // return response()->json(['reponse' => true,'msg' => count($fidels).' Fidèle(s) affecté dans le smallgroup '.$quota->nom.'
                                    // de Metamorpho avec comme mentor '.$mentor->name.'-'.$mentor->prenom]);
                                
                                } else {
                                    return response()->json(['reponse' => false,'msg' => 'Erreur d\'inscription à Metamorpho']);
                                }
                    }
            }
            
        }else{
             return response()->json(['reponse' => false,'msg' => 'Impossible car aucune session de metamorpho n\'est ouverte pour le moment!']);
           }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\metamorpho  $metamorpho
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $MentorDispo=User::where([['visible','=','1'],['mentor','=','1'],['disponible','=','1']])
        ->whereNotExists(function($query)
                {
                    $query->select('smallgroups.quota')
                          ->from('smallgroups')
                          ->whereRaw('users.id = smallgroups.user_id');
                })
        ->get();
        $smlg=smallgroup::selectRaw('Users.name,Users.prenom,Users.sexe,Users.telephone,smallgroups.*, metamorphos.session')
        ->join('users','users.id','smallgroups.user_id')
        ->join('metamorphos','metamorphos.id','smallgroups.metamorpho_id')->get();
        
        $smallFidel=smallgroup::with(['fidel','metamorpho','user'])->get();
        
        $smalgBy=smallgroup::find($id);
      //  dd($smalgBy);

        return view('pages.meta.addSmallgroup',compact('MentorDispo','smlg','smallFidel','smalgBy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\metamorpho  $metamorpho
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $active=metamorpho::where('etat','active')->first();
        if (!$active) {
            $ouv=metamorpho::where('id',$id)->first();
          $ouvsession= $ouv->update([
                'etat' => 'active',
            ]);
            if($ouvsession){
                return response()->json(['reponse' => true,'msg','La session '.$ouv->session.' est ouvert']);
            }else{
                return response()->json(['reponse' => false,'msg','Erreur du modification!']);
            }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Une seesion est active, impossible d\'en a avoir 2 simultanement!']);

        }
    }
    public function cloturer($id)
    {
        $active=metamorpho::where('id',$id)->first();
        if ($active) {
            $closession= $active->update([
                'etat' => 'cloturer',
            ]);
            if($closession){
                return response()->json(['reponse' => true,'msg','La session '.$active->session.' est cloturer']);
            }else{
                return response()->json(['reponse' => false,'msg','Erreur du modification!']);
            }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Aucune session trouvée!!']);

        }
    }
    public function suspendre($id)
    {
        $active=metamorpho::where('id',$id)->first();
        if ($active) {
            $closession= $active->update([
                'etat' => 'suspendu',
            ]);
            if($closession){
                return response()->json(['reponse' => true,'msg','La session '.$active->session.' est suspendu']);
            }else{
                return response()->json(['reponse' => false,'msg','Erreur du modification!']);
            }
        }else{
            return response()->json(['reponse' => false,'msg' => 'Aucune session trouvée!!']);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\metamorpho  $metamorpho
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $smallFidel=smallgroup::with(['fidel','metamorpho','user'])->get();

        $sm=smallgroup::findOrFail($id);
        if($sm){
            $sm->nom=$request->nom;
            $sm->quota=$request->quota;
            $sm->user_id=$request->user_id;
            // $sm->metamorpho_id=$request->metamorpho_id;
            $sm->save();
            return redirect('/addSmallgroup')->with('message','Modification réussit');
        }else{
            return back()->with('message','Erreur');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\metamorpho  $metamorpho
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fidelExiste=fidel::where('smallgroup_id',$id)->get();
        if ($fidelExiste->count()>0) {
            return response()->json([
                'reponse' => false,
                'msg' => 'Suppression impossible, car ce smallgroup contient au-moins 1 mentore!!',
            ]);
        } else {
           $sm=smallgroup::find($id);
           $sm->delete();
           if ($sm) {
            return response()->json([
                'reponse' => true,
                'msg' => 'Suppression réussie',
            ]);
           } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Erreur de suppression',
            ]);
           }
        }
    }
    public function viderSmallgroup($id)
    {
        $fidelExiste=fidel::where('smallgroup_id',$id)->get();
        if ($fidelExiste->count()>0) {    
            // dd($fidelExiste->id);
            $sm='';       
            foreach($fidelExiste as $f) {
                $sm=fidel::find($f->id); 
                $sm->smallgroup_id=null;
                $sm->save();
            }
           if ($sm) {
            return response()->json([
                'reponse' => true,
                'msg' => 'Smallgroup vider',
            ]);
           } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Erreur',
            ]);
           }
        } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Impossible, car ce smallgroup ne contient pas mentore!!',
            ]);
           
        }
    }
    public function sendEmails($id)
    {
        $fid=new FidelController();
        $fidelExiste=fidel::where('smallgroup_id',$id)->get();
        if ($fidelExiste->count()>0) {    
            // dd($fidelExiste->id);
            $sm='';       
            $i=0;
            foreach($fidelExiste as $f) { 
                   $sm= $fid->mail($f->email,$f,'Metamorpho, Info du smallgroupe');    
                $i++;
              if(  $fidelExiste->count()==$i){
                $sm=true;
              }
            }
           if ($sm) {
            return response()->json([
                'reponse' => true,
                'msg' => 'Un email est envoyer à '.$fidelExiste->count().' Mentore(s).',
            ]);
           } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Erreur email',
            ]);
           }
        } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Impossible, car ce smallgroup ne contient pas mentore!!',
            ]);
           
        }
    }
    public function sendEmail($id)
    {
        $fid=new FidelController();
        $fidelExiste=fidel::find($id);
        if ($fidelExiste->count()>0) {    
            $sm=''; 
            $fid->mail($fidelExiste->email,$fidelExiste,'Metamorpho, Info du smallgroupe'); 
            $sm=true;
           if ($sm) {
            return response()->json([
                'reponse' => true,
                'msg' => 'Un email est envoyer au  mentore '.$fidelExiste->prenom.'-'.$fidelExiste->nom,
            ]);
           } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Erreur email',
            ]);
           }
        } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Impossible, car ce smallgroup ne contient pas mentore!!',
            ]);
           
        }
    }
    public function detacher($id)
    {
        $fidelExiste=fidel::find($id);
        if ($fidelExiste->count()>0) {    
            // dd($fidelExiste->id);
            $sm='';       
                $fidelExiste->smallgroup_id=null;
                $sm=$fidelExiste->save();
           if ($sm) {
            return response()->json([
                'reponse' => true,
                'msg' => 'Mentore détacher du smallgroup',
            ]);
           } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Erreur',
            ]);
           }
        } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Impossible, Erreur',
            ]);
           
        }
    }
    
}