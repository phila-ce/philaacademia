<?php

namespace App\Http\Controllers;

use App\Models\reponseExament;
use Illuminate\Http\Request;

class ReponseExamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\reponseExament  $reponseExament
     * @return \Illuminate\Http\Response
     */
    public function show(reponseExament $reponseExament)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\reponseExament  $reponseExament
     * @return \Illuminate\Http\Response
     */
    public function edit(reponseExament $reponseExament)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\reponseExament  $reponseExament
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, reponseExament $reponseExament)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\reponseExament  $reponseExament
     * @return \Illuminate\Http\Response
     */
    public function destroy(reponseExament $reponseExament)
    {
        //
    }
}
