<?php

namespace App\Http\Controllers;

use App\Models\fideleLangueSession;
use Illuminate\Http\Request;

class FideleLangueSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fideleLangueSession  $fideleLangueSession
     * @return \Illuminate\Http\Response
     */
    public function show(fideleLangueSession $fideleLangueSession)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fideleLangueSession  $fideleLangueSession
     * @return \Illuminate\Http\Response
     */
    public function edit(fideleLangueSession $fideleLangueSession)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\fideleLangueSession  $fideleLangueSession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fideleLangueSession $fideleLangueSession)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fideleLangueSession  $fideleLangueSession
     * @return \Illuminate\Http\Response
     */
    public function destroy(fideleLangueSession $fideleLangueSession)
    {
        //
    }
}
