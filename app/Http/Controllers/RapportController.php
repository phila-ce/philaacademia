<?php

namespace App\Http\Controllers;

use App\Models\rapport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\fidelMetamorpho;

class RapportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $ok= Validator::make($request->all(),[
            'fidel_id'=>'required',
            'debut'=>'required',
            'comment_debut'=>'required',
            'fin'=>'required',
            'comment_fin'=>'required',
            'avis'=>'required',
            'observation'=>'required',
        ]);
        if(!$ok->fails()){
            $fidel= rapport::firstOrCreate(
                [
                    'user_id'=>Auth::user()->id,
                    'fidel_id'=>$request->fidel_id,
                ],
                [
                    'debut'=>$request->debut,
                    'coment_debut'=>$request->comment_debut,
                    'fin'=>$request->fin,
                    'coment_fin'=>$request->comment_fin,
                    'avis'=>$request->avis,
                    'observation'=>$request->observation,
                ]
             );
        $metafin=fidelMetamorpho::where('fidel_id',$fidel->fidel_id)->first();
        $metafin->etat='1';
        $metafin->save();
        if ($metafin) {
            return response()->json(['reponse' => true,'msg' => 'Rapport envoyer avec succès']);
        } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur']);
        }

}else{
    // dd($ok->getMessageBag());
    return response()->json(['reponse' => false,'msg' => 'erreur validation '.$ok->getMessageBag()]);
}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function show(rapport $rapport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function edit(rapport $rapport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, rapport $rapport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function destroy(rapport $rapport)
    {
        //
    }
}
