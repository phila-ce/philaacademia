<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\fidel;
use App\Models\mentor;
use App\Mail\philaMail;
use App\Models\rapport;
use App\Models\smallgroup;
use App\Mail\PhilaUserMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\FidelController;
use Illuminate\Database\Schema\MySqlBuilder;
use App\Models\ecap;

class MentorController extends Controller
{
    public function mail($email,User $user,$subject)
    {        
        Mail::to($email)->send(new PhilaUserMail($user,$subject));      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $allUser=User::where('visible','=','1')->get();
        $devenirMentor=User::where([['visible','=','1'],['mentor','=','0'],['disponible','=','1']])->get();
        $devenirProf=User::where([['visible','=','1'],['ecap','=','0'],['disponible','=','1']])->get();
        
        return view('pages.meta.g_user',compact('allUser','devenirMentor','devenirProf'));
    }
    public function passerVerEcap()
    {
        $sessionEcap=ecap::where('etat','active')->first();
         $fidelsInterne=fidel::selectRaw('fidels.*')
         ->join('rapports','rapports.fidel_id','fidels.id')
         ->where([['fidels.visible','=','1'],['etat_activite','active'],['est_de','Phila'],['smallgroup_id','<>',null]])->get();
         $fidelsExterne=fidel::selectRaw('fidels.*')
         ->where([['fidels.visible','=','1'],['etat_activite','enregistrer'],
         ['est_de','Exterieur'],['smallgroup_id',null]])->get();
        // dd($fidelsExterne);
        return view('pages.meta.envoyerVersEcap',compact('fidelsInterne','fidelsExterne','sessionEcap'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function MySmallgroup()
    {
        // $mentor=User::where('user_id',Auth::user()->id)->get();
        if(Auth::user()->mentor==1){
            $MySmallgroup=smallgroup::with('fidel')->where('user_id',Auth::user()->id)->get();
            // dd($MySmallgroup);
            return view('pages.meta.mySmallgroup',compact('MySmallgroup'));
        }else{
            return back()->with('message','Impossible ');
        }
    }
    public function viewPageRapport()
    {
        // $mentor=User::where('user_id',Auth::user()->id)->get();
        if(Auth::user()->mentor==1){
            $MySmallgroup=smallgroup::with('fidel','metamorpho')->where('user_id',Auth::user()->id)->get();
            // dd($MySmallgroup[0]->fidel[0]->metamorpho[0]->pivot->etat);
            return view('pages.meta.sendRapport',compact('MySmallgroup'));
        }else{
            return back()->with('message','Impossible ');
        }
    }
    
    public function PageGestionRapport()
    {
       
            $rapports=rapport::with('user','fidel')->selectRaw('rapports.*,metamorphos.session,smallgroups.nom')
            ->join('smallgroups','smallgroups.user_id','rapports.user_id')
            ->join('metamorphos','metamorphos.id','smallgroups.metamorpho_id')
            ->where([['metamorphos.etat','active'],['metamorphos.etat','active']])->get();
            // dd($rapports);
            return view('pages.meta.gRapport',compact('rapports'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->tabUser);
        $rap=false;
          $users=explode(',',$request->tabUser);
            for($i=0;$i<count($users);$i++){
            // $rep=User::updateOrCreate([
            //     'cultive_id'=>$active->id,
            //     'fidel_id'=>$users[$i],
            //   ]);
                $rap =User::findOrFail($users[$i]);
                
                    $rap->update([
                        'mentor'=>'1'
                    ]);  
                $rap?self::mail($rap->email,$rap,'Affecté comme mentor'):'';
          }

          if ($rap) {
            return response()->json(['reponse' => true,'msg' => count($users).' Ouvrier(s) sont affecté pour devenir mentor, et recevrons un mail de notification']);
          } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur d\'affectation']);
          }
    }
    public function becomeProf(Request $request)
    {
       //  dd($request->tabUserOuv);
        $rap=false;
          $users=explode(',',$request->tabUserOuv);
            for($i=0;$i<count($users);$i++){
                $rap =User::findOrFail($users[$i]);
                
                    $rap->update([
                        'ecap'=>'1'
                    ]);  
                $rap?self::mail($rap->email,$rap,'Affecté comme enseignant à l\'ECAP'):'';
          }

          if ($rap) {
            return response()->json(['reponse' => true,'msg' => count($users).' Ouvrier(s) sont affecté dans l\'équipe des enseigant de l\'ECAP, et recevrons un mail de notification']);
          } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur d\'affectation ECAP']);
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function show(mentor $mentor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function edit(mentor $mentor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mentor $mentor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userExiste=User::find($id);
        if ($userExiste->count()>0) {  
            $sm=$userExiste->delete();
           if ($sm) {
            return response()->json([
                'reponse' => true,
                'msg' => 'Mentore détacher du smallgroup',
            ]);
           } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Erreur',
            ]);
           }
        } else {
            return response()->json([
                'reponse' => false,
                'msg' => 'Impossible, Erreur',
            ]);
           
        }
    }
}
