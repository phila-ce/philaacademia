<?php

namespace App\Http\Controllers;

use App\Models\smallgroup;
use Illuminate\Http\Request;

class SmallgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\smallgroup  $smallgroup
     * @return \Illuminate\Http\Response
     */
    public function show(smallgroup $smallgroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\smallgroup  $smallgroup
     * @return \Illuminate\Http\Response
     */
    public function edit(smallgroup $smallgroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\smallgroup  $smallgroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, smallgroup $smallgroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\smallgroup  $smallgroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(smallgroup $smallgroup)
    {
        //
    }
}
