<?php

namespace App\Http\Controllers;

use App\Models\difted;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class GiftedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.gifted.gifted');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\difted  $difted
     * @return \Illuminate\Http\Response
     */
    public function show(difted $difted)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\difted  $difted
     * @return \Illuminate\Http\Response
     */
    public function edit(difted $difted)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\difted  $difted
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, difted $difted)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\difted  $difted
     * @return \Illuminate\Http\Response
     */
    public function destroy(difted $difted)
    {
        //
    }
}
