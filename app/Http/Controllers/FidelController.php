<?php

namespace App\Http\Controllers;

use App\Models\cultive;
use App\Models\cultiveFidel;
use App\Models\fidel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\philaMail;

class FidelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.accueil');
    }
    public function mail($email,fidel $fidel,$subject)
    { 
        // $user=['email'=>'silasjmas@gmail.com','nom'=>'Silas masimango'];
        // Mail::to('silasjmas@gmail.com')->send(new philaMail($user));
        // return view('auth.login');
         
            Mail::to($email)->send(new philaMail($fidel,$subject));
      
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $ok= Validator::make($request->all(),[
            'email'=>'unique:fidels',
            'phone'=>'unique:fidels',
        ]);
        if(!$ok->fails()){
            $fidel= fidel::firstOrCreate(
            [
                'phone'=>$request->phone==''?null:$request->phone,
                'email'=>$request->email==''?null:$request->email,
            ], 
            [
                'nom'=>$request->nom,
                'prenom'=>$request->prenom,
                'sexe'=>$request->sexe,
                'datenaissance'=>$request->datenaissance,
                'lieu'=>$request->lieu,
                'etatCivil'=>$request->etatCivil,
                'baptiser'=>$request->baptiser,
                'etat_activite'=>'enregistrer',
                'commune'=>$request->commune,
                'quartier'=>$request->qurtier,
                'avenue'=>$request->avenue,
                'est_de'=>$request->est_de,
                'user_id'=>Auth::user()->id,
            ]
        );
        if ($fidel) {            
            if ($request->email!='' ) {
                return response()->json(['reponse' => true,'msg' => 'Fidèl enregistrer, un mail est envoyer chez le fidèle pour le confirmer son inscription!']);
            } else {
                return response()->json(['reponse' => true,'msg' => 'Fidèl enregistrer!']);
            }
        } else {
            return response()->json(['reponse' => false,'msg' => 'Erreur d\'enregistrement du fidèle!']);
        }

}else{
    return response()->json(['reponse' => false,'msg' => $ok->getMessageBag()]);
}

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fidel  $fidel
     * @return \Illuminate\Http\Response
     */
    public function allFidel(fidel $fidel)
    {
        $fidels=fidel::where([['visible','=','1'],['etat_activite','<>','suspendu'],['est_de','Phila']])->get();
        // dd($fidels);
        $fidelsExterieur=fidel::where([['visible','=','1'],['etat_activite','<>','suspendu'],['est_de','Exterieur']])->get();
        $fidelsFin=fidel::where([['visible','=','1'],['etat_activite','=','fini']])->get();
        $fidelsSuspendu=fidel::where([['visible','=','1'],['etat_activite','=','suspendu']])->get();
        $fidelexport=fidel::where('visible','=','1')->get();
        // foreach ($fidelexport->flatMap->cultive as $podcast) {
        //     dd($podcast->cvc->created_at);
        // }
        // dd($fidelexport[0]->cultives[0]->pivot->etat);

        return view('pages.cvc.allFidels',compact('fidels','fidelsFin','fidelexport','fidelsSuspendu','fidelsExterieur'));
    }
    public function show($id)
    {
        $fidel=fidel::where('id',$id)->first();
        //  dd($fidel->metamorpho->isEmpty());
        return view('pages.fidelBy',compact('fidel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fidel  $fidel
     * @return \Illuminate\Http\Response
     */
    public function edit(fidel $fidel)
    {
        //
    }
    public function suspendFidel(Request $request)
    {
        $rap =fidel::findOrFail($request->fidel_id);
        if($rap){
            $rap->update([
                'etat_activite'=>$request->mode
            ]);
            return back()->with('message','Fidèle suspndu avec succès');
        }else{
            return back()->with('message','Action échouer');
        }

    }
    public function debutCursus(Request $request)
    {
        $active=cultive::where('etat','active')->first();
        if($active){
            $rap =fidel::findOrFail($request->fidel_id);
            if($rap){
                $rap->update([
                    'etat_activite'=>$request->mode
                ]);
                cultiveFidel::updateOrCreate([
                    'cultive_id'=>$active->id,
                    'fidel_id'=>$request->fidel_id,
                ]);
                self::mail($rap->email,$rap,'Inscription à CULTIVER LA VIE DE CHRIST');
                return back()->with('message','Le cursus du Fidèle est actif!');
            }else{
                return back()->with('message','Action échouer');
            }
        }else{
            return back()->with('message','Aucune session ouverte pour le moment!');
          
        }


    } 
    public function debloquerFidel(Request $request)
    {
        $rap =fidel::findOrFail($request->fidel_id);
        if($rap){
            $active =cultiveFidel::where('fidel_id',$request->fidel_id)->first();
            if ($active) {
                $rap->update([
                    'etat_activite'=>'active'
                ]);
                return back()->with('message','Fidèle débloquer avec succès');
            } else {
                $rap->update([
                    'etat_activite'=>'enregister'
                ]);
                return back()->with('message','Fidèle débloquer avec succès');
            }


        }else{
            return back()->with('message','Action échouer');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\fidel  $fidel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fidel $fidel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fidel  $fidel
     * @return \Illuminate\Http\Response
     */
    public function destroy(fidel $fidel)
    {
        //
    }
}
