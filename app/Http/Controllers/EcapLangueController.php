<?php

namespace App\Http\Controllers;

use App\Models\ecapLangue;
use Illuminate\Http\Request;

class EcapLangueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ecapLangue  $ecapLangue
     * @return \Illuminate\Http\Response
     */
    public function show(ecapLangue $ecapLangue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ecapLangue  $ecapLangue
     * @return \Illuminate\Http\Response
     */
    public function edit(ecapLangue $ecapLangue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ecapLangue  $ecapLangue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ecapLangue $ecapLangue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ecapLangue  $ecapLangue
     * @return \Illuminate\Http\Response
     */
    public function destroy(ecapLangue $ecapLangue)
    {
        //
    }
}
