<?php

namespace App\Http\Controllers;

use App\Models\partieCour;
use Illuminate\Http\Request;

class PartieCourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\partieCour  $partieCour
     * @return \Illuminate\Http\Response
     */
    public function show(partieCour $partieCour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\partieCour  $partieCour
     * @return \Illuminate\Http\Response
     */
    public function edit(partieCour $partieCour)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\partieCour  $partieCour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, partieCour $partieCour)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\partieCour  $partieCour
     * @return \Illuminate\Http\Response
     */
    public function destroy(partieCour $partieCour)
    {
        //
    }
}
