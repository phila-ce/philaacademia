<?php

namespace App\Http\Controllers;

use App\Models\reponseTp;
use Illuminate\Http\Request;

class ReponseTpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\reponseTp  $reponseTp
     * @return \Illuminate\Http\Response
     */
    public function show(reponseTp $reponseTp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\reponseTp  $reponseTp
     * @return \Illuminate\Http\Response
     */
    public function edit(reponseTp $reponseTp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\reponseTp  $reponseTp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, reponseTp $reponseTp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\reponseTp  $reponseTp
     * @return \Illuminate\Http\Response
     */
    public function destroy(reponseTp $reponseTp)
    {
        //
    }
}
