<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use phpDocumentor\Reflection\Types\Array_;
use App\Models\User;
use App\Models\fidel;

class PhilaUserMail extends Mailable
{
    use Queueable, SerializesModels;
    public User $data;
    public $sujet='';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $sujet)
    {
        $this->data=$user;
        $this->sujet=$sujet;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->subject($this->sujet)->view('emails.test');
    }
}
