<?php

namespace App\Models;

use App\Models\metamorpho;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class mentor extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function metamorpho(){
        return $this->hasMany(metamorpho::class)->withPivot('fidel_id','etat','observation')->withTimestamps();;
    }
}
