<?php

namespace App\Models;

use App\Models\fidel;
use App\Models\ecapLangue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class fideleLangueSession extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function fidels(){
       return $this->belongsTo(fidel::class);
    }
    public function langueSession(){
       return $this->belongsTo(ecapLangue::class);
    }
}
