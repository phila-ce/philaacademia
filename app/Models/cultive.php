<?php

namespace App\Models;

use App\Models\fidel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class cultive extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function fidels(){
        // return $this->belongsToMany(fidel::class,'cultive_fidels')->withPivot('etat','observation')->withTimestamps();
        return $this->belongsToMany(fidel::class,'cultive_fidels');
    }
}
