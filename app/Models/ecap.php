<?php

namespace App\Models;

use App\Models\fidel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ecap extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function fidel(){
        return $this->belongsToMany(langue::class);
    }
}
