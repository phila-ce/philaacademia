<?php

namespace App\Models;

use App\Models\chapitre;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class examensCours extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function cour(){
       return $this->belongsTo(chapitre::class);
    }
}
