<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class cultiveFidel extends Model
{
    public $incrementing = true;
    use HasFactory;
    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function fidel(){
        return $this->belongsTo(Fidel::class);
    }
    public function cultive(){
        return $this->belongsTo(Cultive::class);
    }
}
