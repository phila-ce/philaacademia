<?php

namespace App\Models;

use App\Models\ecap;
use App\Models\User;
use App\Models\gifted;
use App\Models\cultive;
use App\Models\rapport;
use App\Models\metamorpho;
use App\Models\smallgroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class fidel extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $dates=['created_at','updated_at'];
    protected $with=['cultives','gifted','metamorpho','ecap'];

    public function user(){
        return $this->belongsTo(User::class); 
    }
    public function ecap(){
        return $this->belongsToMany(ecap::class,'fidel_ecaps')->withPivot('etat','observation')->withTimestamps();
    }
    public function metamorpho(){
        return $this->belongsToMany(metamorpho::class,'fidel_metamorphos')->withPivot('etat')->withTimestamps();
    }
    public function gifted(){
        return $this->belongsToMany(gifted::class,'fidel_gifteds')->withPivot('etat','observation')->withTimestamps();
    }
    public function cultives(){
        //return $this->belongsToMany(cultive::class,'cultive_fidels');
        return $this->belongsToMany(cultive::class,'cultive_fidels')->withPivot('etat','observation')->withTimestamps();
        // return $this->belongsToMany(cultive::class,'cvc_fd')->as('cvc')->withPivot('etat','observation')->withTimestamps();;
    }
    public function smalgroup(){
        return $this->belongsTo(smallgroup::class);
    }
    public function rapport(){
        return $this->hasMany(rapport::class);
    }
}
