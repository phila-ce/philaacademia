<?php

namespace App\Models;

use App\Models\ecap;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ecapLangue extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function ecap(){
       return $this->belongsTo(ecap::class);
    }
    public function langue(){
       return $this->belongsTo(langue::class);
    }
}
