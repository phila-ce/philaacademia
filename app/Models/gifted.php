<?php

namespace App\Models;

use App\Models\fidel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class gifted extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function fidel(){
        return $this->belongsToMany(fidel::class)->withPivot('etat','observation')->withTimestamps();;
    }
}
