<?php

namespace App\Models;

use App\Models\fidel;
use App\Models\mentor;
use App\Models\smallgroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class metamorpho extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $dates=['created_at','updated_at'];

    public function fidel(){
        return $this->belongsToMany(fidel::class)->withPivot('etat','observation')->withTimestamps();
    }
    public function mentor(){
        return $this->belongsTo(mentor::class);
    }
    public function smallgroup(){
        return $this->hasMany(smallgroup::class);
    }
}
