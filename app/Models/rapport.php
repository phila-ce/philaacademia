<?php

namespace App\Models;

use App\Models\User;
use App\Models\fidel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class rapport extends Model
{
    use HasFactory;
    
    protected $guarded=[];

    public function fidel(){
        return $this->belongsTo(fidel::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
