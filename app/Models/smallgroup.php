<?php

namespace App\Models;

use App\Models\User;
use App\Models\fidel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class smallgroup extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function fidel(){
        return $this->hasMany(fidel::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function metamorpho(){
        return $this->belongsTo(metamorpho::class);
    }
}
