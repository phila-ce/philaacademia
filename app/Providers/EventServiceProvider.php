<?php

namespace App\Providers;

use App\Models\fidel;
use Illuminate\Http\Request;
use App\Observers\fidelObserver;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Models\cultiveFidel;
use App\Observers\cvcObserver;
use App\Models\fidelMetamorpho;
use App\Observers\metafidelObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        fidel::observe(fidelObserver::class);
        cultiveFidel::observe(cvcObserver::class);
        fidelMetamorpho::observe(metafidelObserver::class);
    }
}
