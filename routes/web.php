<?php

use App\Http\Controllers\CultiveController;
use App\Http\Controllers\EcapController;
use App\Http\Controllers\FidelController;
use App\Http\Controllers\GiftedController;
use App\Http\Controllers\MetamorphoController;
use App\Models\fidel;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MentorController;
use App\Http\Controllers\RapportController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pagefidels.index');
})->name('accueil');
Route::get('/mesCours', function () {
    return view('pagefidels.formations');
})->name('mesCours');
Route::get('/detail', function () {
    return view('pagefidels.detailFormation');
})->name('detail');
Route::get('/voirLecon', function () {
    return view('pagefidels.cour');
})->name('voirLecon');
Route::get('/loginFidel', function () {
    return view('pagefidels.login');
})->name('loginFidel');
Route::get('/registerFidel', function () {
    return view('pagefidels.inscription');
})->name('registerFidel');
Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::get('/testmail', [FidelController::class,'mail'])->name('testmail');
 
Route::middleware(['auth'])->group( function (){
    Route::get('/dashboard', [FidelController::class,'index'])->name('dashboard');
    Route::get('/addFidele', [FidelController::class,'addFidele'])->name('addFidele');
    Route::get('/cultive', [CultiveController::class,'index'])->name('cultive');
    Route::get('/metamorpho', [MetamorphoController::class,'index'])->name('metamorpho');
 
    //la page pour afficher les candidat à Gifted
    Route::get('/gifted', [GiftedController::class,'index'])->name('gifted');
    //la page pour afficher les fidels
    Route::get('/fideles', [FidelController::class,'allFidel'])->name('fideles');
    //la page pour debut CVC
    Route::get('/g_cultive', [CultiveController::class,'create'])->name('g_cultive'); 
    //la page pour debut de metamorpho
    Route::get('/debutMeta', [CultiveController::class,'debutMeta'])->name('debutMeta'); 
    //la page pour la gestion CVC
    Route::get('/gestioncvc', [CultiveController::class,'gestioncvc'])->name('gestioncvc'); 
    //la page pour la gestion des users
    Route::get('/g_user', [MentorController::class,'index'])->name('g_user'); 

    //ouvrir la session de CVC
    Route::get('ouverture/{id}', [CultiveController::class,'edit'])->name('ouverture');
    //Cloturer la session de CVC
    Route::get('cloturer/{id}', [CultiveController::class,'cloturer'])->name('cloturer');
    //Suspendre la session de CVC
    Route::get('suspendre/{id}', [CultiveController::class,'suspendre'])->name('suspendre');


    //debut route de metamorpho

    //afficher la page de la creation de session de metamorpho
    Route::get('/metamorpho', [MetamorphoController::class,'index'])->name('metamorpho');
    //afficher page de la creation des mentors
    Route::get('/addmentor', [MetamorphoController::class,'addmentor'])->name('addmentor');
    //afficher page de la gestion des mentors
    Route::get('/gMentor', [MetamorphoController::class,'gMentor'])->name('gMentor');
    //afficher page de la creation des smallgroup
    Route::get('/addSmallgroup', [MetamorphoController::class,'addSmallgroup'])->name('addSmallgroup');
    //afficher page de la affectation dans les smallgroup
    Route::get('/gestionSmallgroup', [MetamorphoController::class,'gestionSmallgroup'])->name('gestionSmallgroup');
    //afficher page de la affectation dans les smallgroup
    Route::get('/gRapport', [MetamorphoController::class,'gRapport'])->name('gRapport');
    //afficher page passer à ECAP
    Route::get('/envoyerEcap', [MetamorphoController::class,'envoyerEcap'])->name('envoyerEcap');
  //ouvrir la session de Metamorpho
  Route::get('ouvertureMeta/{id}', [MetamorphoController::class,'edit'])->name('ouvertureMeta');
  //Cloturer la session de Metamorpho
  Route::get('cloturerMeta/{id}', [MetamorphoController::class,'cloturer'])->name('cloturerMeta');
  //Suspendre la session de Metamorpho
  Route::get('suspendreMeta/{id}', [MetamorphoController::class,'suspendre'])->name('suspendreMeta');
    //Afficher la page de mofication de small group
  Route::get('editeSmallgroup/{id}', [MetamorphoController::class,'show'])->name('editeSmallgroup');
    //Supprimer un small group
  Route::get('deleteSmallgroup/{id}', [MetamorphoController::class,'destroy'])->name('deleteSmallgroup');
    //Vider un small group
  Route::get('viderSmallgroup/{id}', [MetamorphoController::class,'viderSmallgroup'])->name('viderSmallgroup');
    //detacher un mentore du small group
  Route::get('detacheMentore/{id}', [MetamorphoController::class,'detacher'])->name('detacheMentore');
    //Envoyer un email aux mentores d'un smallgroup
  Route::get('sendEmails/{id}', [MetamorphoController::class,'sendEmails'])->name('sendEmails');
    //Envoyer un email à un mentore d'un smallgroup
  Route::get('sendEmail/{id}', [MetamorphoController::class,'sendEmail'])->name('sendEmail');
    //Afficher le smallgroup du mentor
  Route::get('MySmallgroup', [MentorController::class,'MySmallgroup'])->name('MySmallgroup');
    //Afficher la page d'envoi de rapport
  Route::get('viewPageRapport', [MentorController::class,'viewPageRapport'])->name('viewPageRapport');
    //Afficher la page de gestion des rapports
  Route::get('viewPageGestionRapport', [MentorController::class,'PageGestionRapport'])->name('viewPageGestionRapport');
    //Afficher la page de gestion des rapports
  Route::get('debutEcap', [MentorController::class,'passerVerEcap'])->name('debutEcap');

    //action pour crée une session de Metamorpho
    Route::post('createSessionMeta', [MetamorphoController::class,'store'])->name('createSessionMeta');
        //action pour cree un small group
     Route::post('createSmllgroup', [MetamorphoController::class,'createSmllgroup'])->name('createSmllgroup');
        //action pour affectater dans un smallgroup
     Route::post('affectationSmalg', [MetamorphoController::class,'affectationSmalg'])->name('affectationSmalg');
        //action pour modifier un smallgroup
     Route::post('updateSmallgroup/{id}', [MetamorphoController::class,'update'])->name('updateSmallgroup');
     //action pour modifier un smallgroup
     Route::post('sendRapport', [RapportController::class,'store'])->name('sendRapport');
     
     
     //Fin des route metamorpho
     
     //Début ecap
     
        Route::get('/ecap', [EcapController::class,'index'])->name('ecap');
     
     //FIn ecap



     
     //Afficher les details d'un fidèle
    Route::get('fidelBy/{id}', [FidelController::class,'show'])->name('fidelBy');
    //enregistrement du fidel
    Route::post('addFidel', [FidelController::class,'store'])->name('addFidel');
        //action pour crée une session de CVC
    Route::post('createSessionCVC', [CultiveController::class,'store'])->name('createSessionCVC');
        //action pour inscrire à CVC
    Route::post('debutCvc', [CultiveController::class,'debutCvc'])->name('debutCvc');
    //action pour inscrire à métamorpho
    Route::post('debutMetaAffectation', [CultiveController::class,'debutMetaAffectation'])->name('debutMetaAffectation');
      //action pour suspendre le fidèle
    Route::post('suspendFidel', [FidelController::class,'suspendFidel'])->name('suspendFidel');
       //action pour inscrire à CVC
    Route::post('debutCursus', [FidelController::class,'debutCursus'])->name('debutCursus');
        //action pour bloquer le fidèl
    Route::post('debloquerFidel', [FidelController::class,'debloquerFidel'])->name('debloquerFidel');


        //action pour creer un mentor
    Route::post('becomeMentor', [MentorController::class,'store'])->name('becomeMentor');
        //action pour creer un enseignat
    Route::post('becomeProf', [MentorController::class,'becomeProf'])->name('becomeProf');
  //Supprimer un user
  Route::get('deleteUser/{id}', [MentorController::class,'destroy'])->name('deleteUser');
  



});


require __DIR__.'/auth.php';