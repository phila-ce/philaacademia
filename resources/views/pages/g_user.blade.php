@extends('layouts.template',['titre'=>"Gestion des users",'titre2'=>"Users"])


@section('autres_style')
<link href="{{asset('css/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('js/parsley/parsley.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/dualListbox/bootstrap-duallistbox.min.css') }}">
<link href="{{ asset('css/dataTables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/switchery/switchery.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    {{-- <li class="active"><a data-toggle="tab" href="#tab-1">Session
                        <span class="label label-success">0</span>
                    </a>
                    </li> --}}
                <li class="{{isset($_GET['p'])?'':'active'}}"><a data-toggle="tab" href="#tab-2">Touts les users
                    <span class="label label-danger">{{$allUser->count()}}</span>    
                    </a>
                </li>
                    <li class="{{isset($_GET['p']) && $_GET['p']==1?'active':''}}"><a data-toggle="tab" href="#tab-3">Devenir mentor                        
                    <span class="label label-danger">{{$devenirMentor->count()}}</span> 
                </a>
            </li>
            <li class="{{isset($_GET['p']) && $_GET['p']==2?'active':''}}"><a data-toggle="tab" href="#tab-4">Devenir Enseigant    
                        <span class="label label-danger">{{$devenirProf->count()}}</span> 
                        </a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <div id="tab-2" class="tab-pane active">
                            <div class="panel-body" id="tab-fidel3">
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class="table-responsive">
                                        <table
                                            class="table table-striped table-bordered table-hover dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Prenom-Nom</th>
                                                    <th>Sexe</th>
                                                    <th>Telephone</th>
                                                    <th>email</th>
                                                    <th>Disponibilité</th>
                                                    <th>Rôle</th>
                                                    <th>Options</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($allUser as $i)
                                                <tr class="gradeX">
                                                    <td><a href="#contact-{{ $i->id }}"
                                                            class="client-link">{{ $i->prenom.'-'.$i->name }}</a></td>
                                                    <td> {{ $i->sexe}}</td>
                                                    <td> {{ $i->telephone }}</td>
                                                    <td> {{ $i->email }}</td>
                                                    <td> 
                                                     @if ($i->disponible=='1')
                                                     <span class="label label-primary">Disponible</span>                                                          
                                                     @else
                                                     <span class="label label-danger">Indisponible</span>                                                          
                                                     @endif
                                                    </td>
                                                    <td>{{ $i->role }}</td>
                                                    <td class="text-center">
                                                        <a class="btn btn-warning btn-rounded btn-xs"
                                                            href="{{ route('editeSmallgroup', ['id' => $i->id]) }}"
                                                            style="margin-bottom:5px">
                                                            <i class="fa fa-edit"></i> Modifier</a>
                                                        <a id='deleteUser' href="{{ $i->id }}"
                                                            class="btn btn-danger btn-rounded btn-xs">
                                                            <i class="fa fa-list"></i> Detail</a>
                                                    </td>
                                                </tr>

                                                @empty
                                                <div class='wrapper-content  animated fadeInRight'>
                                                    <div class="row mt-5">
                                                        <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                            <p class="center small text-center  badge badge-danger">
                                                                Aucun enregistrement
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforelse

                                            </tbody>
                                            <tfoot>
                                                    <th>Prenom-Nom</th>
                                                    <th>Sexe</th>
                                                    <th>Telephone</th>
                                                    <th>email</th>
                                                    <th>Disponibilité</th>
                                                    <th>Rôle</th>
                                                    <th>Options</th>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox" id="tab-cvc">
                                            <div class="ibox-title">
                                                <h5>Devenir mentor pour <b>Metamorpho</b> </h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                                    <div class="sk-cube1"></div>
                                                    <div class="sk-cube2"></div>
                                                </div>
                                                <p>
                                                Selectionez au moin un ouvrier puis validé afin qu'il soit enregistrer comme mentor.
                                                </p>
                            
                                                <form id="formUserMentor" action="#" class="wizard-big">
                                                    @csrf
                                                    <select class="form-control dual_select" multiple name="fidel_id" id='fidel'>
                                                        @forelse ($devenirMentor as $f)
                                                        <option value="{{$f->id}}">{{ $f->name.'-'.$f->prenom.' ( '.$f->sexe.')' }}</option>
                                                        @empty
                            
                                                        @endforelse
                                                    </select>
                                                    <input name="tabUser" id="inpute" hidden/>
                            
                                                    <button class="ladda-button btn btn-sm btn-primary"
                                                    id='ladda-session' data-style="expand-left"
                                                    type="submit">Enregistrer</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                    </div>
                    <div id="tab-4" class="tab-pane">
                       <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox" id="tab-prof">
                                    <div class="ibox-title">
                                        <h5>Devenir enseignant pour <b>ECAP</b> </h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="sk-spinner sk-spinner-wandering-cubes">
                                            <div class="sk-cube1"></div>
                                            <div class="sk-cube2"></div>
                                        </div>
                                        <p>
                                        Selectionez au moin un ouvrier puis validé afin qu'il soit enregistrer comme enseignant.
                                        </p>
                    
                                        <form id="formUserProf" action="#" class="wizard-big">
                                            @csrf
                                            <select class="form-control dual_select" multiple name="fidel_id" id='selectouvrier'>
                                                @forelse ($devenirProf as $f)
                                                <option value="{{$f->id}}">{{ $f->name.'-'.$f->prenom.' ( '.$f->sexe.')' }}</option>
                                                @empty
                    
                                                @endforelse
                                            </select>
                                            <input name="tabUserOuv" id="inputeOuv" hidden/>
                    
                                            <button class="ladda-button btn btn-sm btn-primary"
                                            id='ladda-session' data-style="expand-left"
                                            type="submit">Enregistrer</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div> 
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

@endsection
@section('autres_script')
<script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
<script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
{{-- <script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script> --}}
<script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


<script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
<script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

<script src="{{ asset('js/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('js/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>
<script src="{{asset('js/dataTables/datatables.min.js')}}"></script>

<script src="{{ asset('js/switchery/switchery.js') }}"></script>

<script>
        $(document).ready(function () {
            // var elem = document.querySelector('.js-switch');
            // var switchery = new Switchery(elem, { color: '#1AB394' });
            // var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

            // elems.forEach(function(html) {
            // var switchery = new Switchery(html);
            // });

            $('.dual_select').bootstrapDualListbox({
                selectorMinimalHeight: 160
            });
            
                $('.dataTables-example').DataTable({
                    language: {
                        processing: "Traitement en cours...",
                        search: "Rechercher&nbsp;:",
                        lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                        info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix: "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable: "Aucune donnée disponible dans le tableau",
                        paginate: {
                            first: "Premier",
                            previous: "Pr&eacute;c&eacute;dent",
                            next: "Suivant",
                            last: "Dernier"
                        },
                        aria: {
                            sortAscending: ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    },
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [{
                            extend: 'copy'
                        },
                        {
                            extend: 'csv'
                        },
                        {
                            extend: 'excel',
                            title: 'NewsLetter'
                        },
                        {
                            extend: 'pdf',
                            title: 'NewsLetter'
                        },

                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ]

                });

                $('#fidel').change(function(){
                     $('#inpute').val($(this).val());
                });
                $('#selectouvrier').change(function(){
                    // alert($(this).val());
                     $('#inputeOuv').val($(this).val());
                });
                $('#example1').on('click',function(){
                    // $(this).attr("href");
                    alert($(this).val());
                     $('#inputeOuv').val($(this).val());
                });

                $("#formUserMentor").on("submit", function (e) {
                e.preventDefault();
                        if ($('#fidel').val()!='') {
                        add("#formUserMentor", '#tab-cvc', 'becomeMentor')
                        } else {
                            swal({
                                    title: 'Veillez selectionnez au-moins un ouvrier!',
                                     icon: 'error'
                                })
                        }
                });
                
                $("#formUserProf").on("submit", function (e) {
                     e.preventDefault();
                        if ($('#selectouvrier').val()!='') {
                        add("#formUserProf", '#tab-prof', 'becomeProf')
                        } else {
                            swal({
                                    title: 'Veillez selectionnez au-moins un ouvrier!',
                                    icon: 'error'
                                })
                        }
                });
                $("#deleteUser").on("click", function (e) {
                     e.preventDefault();
                     var id = $(this).attr("href");
                     deleteUser(id, '../deleteUser','Suppression user', "Vous êtes sur le point de supprimer l'ouvrier",'tab-fidel3',true,'')
                       
                });

        });
            
        function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
        }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })
                } else {
                    actualiser();
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })
                    $('#fidel').val('');
                    $(f)[0].reset();

                }

            },
        });

    }

    function modifier(id, url) {
        swal({
            title: "Attention Modification",
            text: "Cette action modifira l'etat de la session, voulez-vous continuer ?",
            icon: 'warning',
            dangerMode: true,
            buttons: {
                cancel: 'Non',
                delete: 'OUI'
            }
        }).then(function (willDelete) {
            if (willDelete) {

                $.ajax({
                    url: url + "/" + id,
                    method: "GET",
                    data: "",
                    success: function (data) {
                        //  load('#tab-session');
                        if (!data.reponse) {
                            swal({
                                title: data.msg,
                                icon: 'error'
                            })

                        } else {
                            swal({
                                title: data.msg,
                                icon: 'success'
                            })
                            actualiser();
                        }
                    },
                });
            } else {
                swal({
                    title: "Action annuler",
                    icon: 'error'
                })
            }
        });
    }

    function deleteUser(id, url,titre, msg,loade,actual,p) {
                swal({
                    title: titre,
                    text: msg,
                    icon: 'warning',
                    dangerMode: true,
                    buttons: {
                        cancel: 'Non',
                        delete: 'OUI'
                    }
                }).then(function(willDelete) {
                    if (willDelete) {
                        load(loade);
                        $.ajax({
                            url: url + "/" + id,
                            method: "GET",
                            data: "",
                            success: function(data) {
                                load(loade);
                                if (!data.reponse) {
                                    swal({
                                        title: data.msg,
                                        icon: 'error'
                                    })

                                } else {
                                    swal({
                                        title: data.msg,
                                        icon: 'success'
                                    })
                                    if(actual==true){
                                        if (p=='') {
                                            actualiser();
                                        } else {                                            
                                            location.replace('g_user?p='+p);
                                        }
                                    }
                                }
                            },
                        });
                    } else {
                        swal({
                            title: "Action annuler",
                            icon: 'error'
                        })
                    }
                });
            }
function actualiser() {
        location.reload();
    }


</script>
@endsection

