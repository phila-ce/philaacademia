@extends('layouts.template',['titre'=>"Enregistrement fidèle",'titre2'=>"ajout"])

@section('autres_style')
<link href="{{asset('css/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('js/parsley/parsley.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-markdown/bootstrap-markdown.min.css') }}">

@endsection
@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body" id="tab-fidel">
                            <div class="ibox-title">
                                <h5>Ce formulaire vous permet d'enregistrer un fidèle</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                @if(session()->has('message'))
                                <div class="col-md-6 col-md-offset-3" >
                                       <div class="alert alert-success alert-dismissable">
                                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                           {{session()->get('message')}}
                                       </div>
                                </div>
                                @endif
                                <div class='row'>
                                    <div class=" col-lg-12 col-sm-12">
                                        <form id="formFidel" method="POST" class="" action="" class='form-group'  data-parsley-validate>
                                            @csrf
                                            <div class="row">
                                                <div>
                                                    <input name="id" hidden value=""/>
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Nom</label>
                                                    <input type="text" placeholder="Entrez le Nom" class="form-control"
                                                        name='nom' required aria-required="true" value=""
                                                        data-parsley-minlength="2" data-parsley-trigger="change">
                                                    @if ($errors->has('nom'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('nom')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Prénom</label>
                                                    <input type="text" placeholder="Entrez le Prénom" value=""
                                                        class="form-control" name='prenom'required aria-required="true" value=""
                                                        data-parsley-minlength="2" data-parsley-trigger="change">
                                                    @if ($errors->has('prenom'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('prenom')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Sexe</label>
                                                    <select class=" form-control" id="sexe" required
                                                        aria-required="true" class="validate"
                                                        data-parsley-trigger="change" name="sexe">

                                                        <option value="" disabled selected >Choisissez le sexe</option>
                                                        <option value="H" >Homme</option>
                                                        <option value="F">Femme</option>
                                                    </select>
                                                    @if ($errors->has('sexe'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('sexe')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Telephone</label>
                                                    <input type="phone" placeholder="(082) 000-0000" value=""
                                                        class="form-control" name='phone' id='phone'
                                                        data-mask="999 999-9999" required aria-required="true"
                                                        data-parsley-minlength="3" data-parsley-trigger="change">
                                                    @if ($errors->has('phone'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('phone')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>E-mail</label>
                                                    <input type="email" placeholder="Adresse mail" class="form-control"
                                                        name='email' >
                                                    @if ($errors->has('email'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Etat civil</label>
                                                    <select class=" form-control" id="" required
                                                        aria-required="true" class="validate"
                                                        data-parsley-trigger="change" name="etatCivil">

                                                        <option value="" disabled selected>Etat civil</option>
                                                        <option value="Célibataire">Célibataire</option>
                                                        <option value="Marié(e)">Marié(e)</option>
                                                        <option value="Divorcé(e)">Divorcé(e)</option>
                                                    </select>
                                                    @if ($errors->has('etatCivil'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('etatCivil')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Lieu de Naissance</label>
                                                    <input type="text" placeholder="Lieu naissance" class="form-control"
                                                        name='lieu' required aria-required="true" value=""
                                                        data-parsley-minlength="2" data-parsley-trigger="change">
                                                    @if ($errors->has('lieu'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('lieu')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>

                                                <div class="col-sm-4 form-group">
                                                    <label>Date de Naissance</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i
                                                                class="fa fa-calendar"></i></span>
                                                        <input type="date" placeholder="Date de naissance" class="form-control"
                                                            data-parsley-trigger="change" data-parsley-trigger="change"  name='datenaissance'
                                                            {{ isset($avocat)?'':'required aria-required="true"' }} >
                                                        @if ($errors->has('datenaissance'))

                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('datenaissance')}}</strong>
                                                        </span>

                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Etes-vous déjà baptisé?</label>
                                                    <select class=" form-control" id="" required
                                                        aria-required="true" class="validate"
                                                        data-parsley-trigger="change" name="baptiser">

                                                        <option value="" disabled selected>Etes-vous déjà baptiseé?</option>
                                                        <option value="1">OUI</option>
                                                        <option value="0">NON</option>
                                                    </select>
                                                    @if ($errors->has('baptiser'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('baptiser')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Commune</label>
                                                    <select class=" form-control" id="" required
                                                        aria-required="true" class="validate"
                                                        data-parsley-trigger="change" name="commune">

                                                        <option disabled selected> --choisissez la commune-- </option>
                                                        <option value="BANDALUNGWA">BANDALUNGWA</option>
                                                        <option value="BARUMBU">BARUMBU</option>
                                                        <option value="BUMBU">BUMBU</option>
                                                        <option value="GOMBE">GOMBE</option>
                                                        <option value="KALAMU">KALAMU</option>
                                                        <option value="KASAVUBU">KASAVUBU</option>
                                                        <option value="KIMBANSEKE">KIMBANSEKE</option>
                                                        <option value="KINSHASA">KINSHASA</option>
                                                        <option value="KINTAMBO">KINTAMBO</option>
                                                        <option value="KISENSO">KISENSO</option>
                                                        <option value="LEMBA">LEMBA</option>
                                                        <option value="LIMETE">LIMETE</option>
                                                        <option value="LINGWALA">LINGWALA</option>
                                                        <option value="MAKALA">MAKALA</option>
                                                        <option value="MALUKU">MALUKU</option>
                                                        <option value="MASINA">MASINA</option>
                                                        <option value="MATETE">MATETE</option>
                                                        <option value="MONT-NGAFULA">MONT-NGAFULA</option>
                                                        <option value="NDJILI">NDJILI</option>
                                                        <option value="NGABA">NGABA</option>
                                                        <option value="NGALIEMA">NGALIEMA</option>
                                                        <option value="NGIRI-NGIRI">NGIRI-NGIRI</option>
                                                        <option value="NSELE">NSELE</option>
                                                        <option value="SELEMBAO">SELEMBAO</option>
                                                    </select>
                                                    @if ($errors->has('commune'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('commune')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Quartier :</label>
                                                    <input type="text" placeholder="Quartier" class="form-control"
                                                        name='quartier' value="">
                                                    @if ($errors->has('quartier'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('quartier')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Avenue :</label>
                                                    <input type="text" placeholder="Avenue" class="form-control"
                                                        name='avenue' value="">
                                                    @if ($errors->has('avenue'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('avenue')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Est de :</label>
                                                    <select class=" form-control" id="sexe" required
                                                        aria-required="true" class="validate"
                                                        data-parsley-trigger="change" name="est_de">
                                                        <option value="Phila" >Phila</option>
                                                        <option value="Exterieur">Exterieur</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label></label>
                                                    <div class="i-checks">
                                                        <label> 
                                                        <input type="checkbox"  name="checksendmail"> <i>
                                                            </i>Notifier en même temps par un email</label></div> 
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label></label>
                                                    <div class="i-checks">
                                                        <label> 
                                                        <input type="checkbox"  name="checksendsms"> <i>
                                                            </i>Notifier en même temps par un SMS</label></div> 
                                                </div>
                                                <div class="col-lg-offset-3 col-lg-6 col-sm-12 form-group">
                                                    
                                                    <div class="col-sm-offset-4 col-sm-5">

                                                        <button class="ladda-button btn btn-sm btn-primary"
                                                            id='ladda-session' data-style="expand-right"
                                                            type="submit">Enregistrer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('autres_script')
<script src="{{ asset('js/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
<script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


<script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
<script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

<script src="{{ asset('js/toastr/toastr.min.js') }}"></script>

<script>
        $(document).ready(function () {
            $("#formFidel").on("submit", function (e) {
            e.preventDefault();
            add("#formFidel", '#tab-fidel', 'addFidel')
        });
        });
 function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
    }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg.phone?'Le numéro '+data.msg.phone:''+''+data.msg.email?data.msg.email:"",
                        icon: 'error'
                    })
                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })

                    $(f)[0].reset();
                }

            },
        });

    }


</script>
@endsection
