@extends('layouts.template',['titre'=>"Mon smallgroup",'titre2'=>"Mon smallgroup"])

@section('autres_style')
    <link href="{{ asset('css/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/parsley/parsley.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dualListbox/bootstrap-duallistbox.min.css') }}">
    <link href="{{ asset('css/dataTables/datatables.min.css') }}" rel="stylesheet">

@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
            <div class="panel-body" id="tab-fidel3">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wandering-cubes">
                        <div class="sk-cube1"></div>
                        <div class="sk-cube2"></div>
                    </div>
                    <div class="panel-group" id="accordion">
                        @forelse ($MySmallgroup as $s)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                            href="#collapse{{ $s->id }}">
                                            {{ '(' . $s->nom . ')' }}
                                            <span class="label label-success">{{ $s->fidel->count() }}</span>
                                        </a>
                                        <span
                                            class="badge badge-warning">{{ $s->quota - $s->fidel->count() == 0 ? 'Quota atteind' : 'Reste ' . $s->quota - $s->fidel->count() }}</span>
                                    </h5>
                                    @if ($s->fidel->count() > 0)
                                        <div class="ibox-tools">
                                            <a class="" id="viderSmall" href=" {{ $s->id }} "
                                                tyle="color: #FFFFFF !important">
                                                <span class="label label-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                    Vider le
                                                    smallgroup</span>
                                            </a>
                                            <a class="" id="sendEmails" href="{{ $s->id }} "
                                                tyle="color: #FFFFFF !important">
                                                <span class="label label-success">
                                                    <i class="fa fa-envelope"></i>
                                                    Envoyer des emails</span>
                                            </a>
                                            {{-- <a class="" id="viderSmall" href=" {{ $s->id }} "
                                                tyle="color: #FFFFFF !important">
                                                <span class="label label-primary">
                                                    <i class="fa fa-envelope-open-o"></i>
                                                    Envoyer SMS</span>
                                            </a> --}}
                                        </div>
                                    @endif
                                </div>
                                <div id="collapse{{ $s->id }}"
                                    class="panel-collapse collapse {{ $loop->first ? 'in' : '' }}">
                                    <div class="panel-body">
                                        <div class="row">
                                            @forelse ($s->fidel as $fidel)
                                                <div class="col-lg-4">
                                                    <div class="contact-box center-version">
                                                        <a href="{{ route('fideles') }}">
                                                            <img alt="image" class="img-circle"
                                                                src="{{ asset('img/default.png') }}">

                                                            <h3 class="m-b-xs">
                                                                <strong>{{ $fidel->prenom . ' ' . $fidel->nom }}</strong>
                                                            </h3>
                                                            <div class="font-bold">
                                                                {{ $fidel->est_de }}
                                                            </div>
                                                            <address class="m-t-md">
                                                                <strong>
                                                                    <span class="label label-success">
                                                                        <strong>{{ $fidel->etatCivil }}</span></strong><br>
                                                                {{ $fidel->avecnue . ',' . $fidel->commune . ',' . $fidel->quartier }}<br>
                                                                <abbr title="Phone"><i class="fa fa-phone"></i>
                                                                    :</abbr>
                                                                {{ $fidel->phone }}<br>
                                                                <abbr title="email"><i class="fa fa-envelope"></i>
                                                                    :</abbr>
                                                                {{ $fidel->email }}<br>
                                                            </address> <br>
                                                            <div class="clearfix">
                                                                <span class="badge badge-warning">
                                                                    {{ $fidel->baptiser == 'OUI' ? 'déjà baptiser' : 'Pas baptiser' }}
                                                                </span>
                                                                <span class="badge badge-primary">
                                                                    {{ $fidel->etat_activite }}
                                                                </span>
                                                            </div>
                                                        </a>
                                                        <div class="contact-box-footer">
                                                            <div class="m-t-xs btn-group">
                                                                {{-- <a class="btn btn-xs btn-white"><i
                                                                        class="fa fa-envelope-open"></i>
                                                                    SMS </a> --}}
                                                                <a href="{{ $fidel->id }}" id="sendEmail"
                                                                    class="btn btn-xs btn-white"><i
                                                                        class="fa fa-envelope"></i>
                                                                    Email</a>
                                                                <a href="{{ $fidel->id }}" id="detacher"
                                                                    class="btn btn-xs btn-white"><i
                                                                        class="fa fa-edit"></i>
                                                                    Raport</a>
                                                                <a href="{{ $fidel->id }}" id="detacher"
                                                                    class="btn btn-xs btn-white"><i
                                                                        class="fa fa-trash-o"></i>
                                                                    Détacher</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <span class="label label-warning">Aucun
                                                    mentore affecté pour le
                                                    moment</span>
                                            @endforelse

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <span class="label label-warning">Aucun
                                smallgroup crée
                                pour le moment</span>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('autres_script')
    <script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
    <script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
    {{-- <script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script> --}}
    <script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


    <script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
    <script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

    <script src="{{ asset('js/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('js/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>
    <script src="{{ asset('js/dataTables/datatables.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('.dataTables-example').DataTable({
                language: {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'NewsLetter'
                    },
                    {
                        extend: 'pdf',
                        title: 'NewsLetter'
                    },

                    {
                        extend: 'print',
                        customize: function(win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

            $(document).on("click", "#deleteSmal", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // alert(idv+'-'+id);
                deleteSmallgroup(id, '../deleteSmallgroup', 'Supprimer ce group',
                    "Vous êtes sur le point de supprimer ce smallgroup", "#tab-fidel3");
            });
            $(document).on("click", "#viderSmall", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // alert(idv+'-'+id);
                deleteSmallgroup(id, '../viderSmallgroup', 'Vider ce group',
                    "Vous êtes sur le point de vidé ce smallgroup de ses mentores", "#tab-fidel3", true);
            });
            $(document).on("click", "#sendEmails", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // alert(idv+'-'+id);
                deleteSmallgroup(id, '../sendEmails', 'Envoi email à un group',
                    "Vous êtes sur le point d\'envoyer un email aux membres de ce smallgroup",
                    '#tab-fidel3', false);
            });
            $(document).on("click", "#sendEmail", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // alert(idv+'-'+id);
                deleteSmallgroup(id, '../sendEmail', 'Envoi email personel',
                    "Vous êtes sur le point d\'envoyer un email à ce mentore", '#tab-fidel3', false);
            });
            $(document).on("click", "#detacher", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // alert(idv+'-'+id);
                deleteSmallgroup(id, '../detacheMentore', 'Attention Détachement',
                    "Vous êtes sur le point de Détacher ce mentore du smallgroup!!", '#tab-fidel3', true
                    );
            });
        });

        function load(id) {
            $(id).children('.ibox-content').toggleClass('sk-loading');
        }

        function add(form, idLoad, url) {
            var f = form;
            var loade = idLoad;
            var u = url;
            load(loade);
            $.ajax({
                url: u,
                method: "POST",
                data: $(f).serialize(),
                success: function(data) {
                    load(loade);
                    if (!data.reponse) {
                        swal({
                            title: data.msg,
                            icon: 'error'
                        })
                    } else {
                        swal({
                            title: data.msg,
                            icon: 'success'
                        })
                        $('#fidel').val('');
                        $(f)[0].reset();
                    }

                },
            });

        }

        function deleteSmallgroup(id, url, titre, msg, loade, actual) {
            swal({
                title: titre,
                text: msg,
                icon: 'warning',
                dangerMode: true,
                buttons: {
                    cancel: 'Non',
                    delete: 'OUI'
                }
            }).then(function(willDelete) {
                if (willDelete) {
                    load(loade);
                    $.ajax({
                        url: url + "/" + id,
                        method: "GET",
                        data: "",
                        success: function(data) {
                            load(loade);
                            if (!data.reponse) {
                                swal({
                                    title: data.msg,
                                    icon: 'error'
                                })

                            } else {
                                swal({
                                    title: data.msg,
                                    icon: 'success'
                                })
                                if (actual == true) {
                                    actualiser();
                                }
                            }
                        },
                    });
                } else {
                    swal({
                        title: "Action annuler",
                        icon: 'error'
                    })
                }
            });
        }

        function actualiser() {
            location.reload();
        }
    </script>
@endsection
