@extends('layouts.template',['titre'=>"Affectation dans smallgroup",'titre2'=>"metamorpho"])



@section('autres_style')
<link href="{{asset('css/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('js/parsley/parsley.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/dualListbox/bootstrap-duallistbox.min.css') }}">

@endsection
@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox" id="tab-cvc">
                <div class="ibox-title">
                    <h5>Affectation dans <b>un smallgroup</b> </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wandering-cubes">
                        <div class="sk-cube1"></div>
                        <div class="sk-cube2"></div>
                    </div>
                    <p>
                       Selectionez au moin un fidèle puis choisissez un smallgroup et enregistrez pour que l'affectation soit valide
                    </p>

                    <form id="formAffectationSml" action="#" class="wizard-big" data-parsley-validate>
                        @csrf
                        <select class="form-control dual_select" multiple name="fidel_id" id='fidel'>
                            @forelse ($fidels as $f)
                            <option value="{{$f->id}}">{{ $f->nom.'-'.$f->prenom.' ( '.$f->phone.')'.'
                                ('.date_diff(new DateTime(),new DateTime($f->datenaissance))->format('%y').' Ans'.')' }}</option>
                            @empty

                            @endforelse
                        </select>
                        <input name="tabFidel" id="inpute" hidden/>
                        <div class="col-sm-12 form-group ">
                            <label>Smallgroup</label>
                            <select class=" form-control" required aria-required="true" class="validate"
                                data-parsley-trigger="change" name="smlgroup">
                                <option value="" disabled selected >Choisissez un smallgroup</option>
                                @forelse ($smlg as $sm)
                                <option value="{{$sm->i}}" >{{$sm->name.'-'.$sm->prenom.'
                                 ('.date_diff(new DateTime(),new DateTime($sm->datenaissace))->format('%y').' Ans'.')
                                 ['.$sm->nom.'] (Quota :'.$sm->quota.')'}}</option>                                    
                                @empty                                    
                                @endforelse
                            </select>
                        </div>
                        {{-- <div class="i-checks"><label> 
                            <input type="checkbox"  name="checksendmail"> <i>
                                </i>Notifier en même temps le mentore </label></div>  --}}
                        <button class="ladda-button btn btn-sm btn-primary"
                        id='ladda-session' data-style="expand-left"
                        type="submit">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('autres_script')
<script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
<script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


<script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
<script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

<script src="{{ asset('js/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('js/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>

<script>
        $(document).ready(function () {
            $("#formFidel").on("submit", function (e) {
            e.preventDefault();

            add("#formFidel", '#tab-fidel', 'debutCvc');
        });
        $('#fidel').change(function(){
            $('#inpute').val($(this).val());
        });
            $("#formAffectationSml").on("submit", function (e) {
            e.preventDefault();
            if ($('#fidel').val()!='') {
               add("#formAffectationSml", '#tab-cvc', 'affectationSmalg')
            } else {
                swal({
                        title: 'Veillez selectionnez au-moins un fidèle!',
                        icon: 'error'
                    })
            }
        });

        $('.dual_select').bootstrapDualListbox({
                selectorMinimalHeight: 160
            });
        });
        function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
        }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })
                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })
                    $('#fidel').val('');
                    $(f)[0].reset();
                }

            },
        });

    }


</script>
@endsection
