@extends('layouts.template',['titre'=>"Session",'titre2'=>"metamorpho"])


@section('autres_style')
    <link href="{{ asset('css/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/parsley/parsley.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-markdown/bootstrap-markdown.min.css') }}">

@endsection
@section('content')

    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-2">Crée la session
                            </a>
                        </li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Gession des session
                                <span class="label label-warning">{{ $session->count() }}</span>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div id="tab-2" class="tab-pane active">
                            <div class="panel-body" id="tab-fidel">
                                <div class="ibox-title">
                                    <h5>Ce formulaire vous permet de créer une session</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                        <div class="sk-cube1"></div>
                                        <div class="sk-cube2"></div>
                                    </div>
                                    <div class='row'>
                                        <div class=" col-lg-12 col-sm-12">
                                            <form id="formFidel" method="POST" class="" action=""
                                                class='form-group' data-parsley-validate>
                                                @csrf
                                                <div class="row">
                                                    <div>
                                                        <input name="id" hidden value="" />
                                                    </div>
                                                    <div class="col-sm-4 form-group ">
                                                        <label>Session</label>
                                                        <input type="text" placeholder="Entrez le Nom "
                                                            class="form-control" name='session' required
                                                            aria-required="true" value="" data-parsley-minlength="2"
                                                            data-parsley-trigger="change">
                                                        @if ($errors->has('session'))

                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('session') }}</strong>
                                                            </span>

                                                        @endif
                                                    </div>
                                                    <div class="col-sm-4 form-group">
                                                        <label>Date de debut</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i
                                                                    class="fa fa-calendar"></i></span>
                                                            <input type="date" placeholder="Date de debut"
                                                                class="form-control" data-parsley-trigger="change"
                                                                data-parsley-trigger="change" name='debut' required
                                                                aria-required="true">
                                                            @if ($errors->has('debut'))

                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('debut') }}</strong>
                                                                </span>

                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 form-group">
                                                        <label>Date de Fin</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i
                                                                    class="fa fa-calendar"></i></span>
                                                            <input type="date" placeholder="Date de Fin"
                                                                class="form-control" data-parsley-trigger="change"
                                                                data-parsley-trigger="change" name='fin' required
                                                                aria-required="true">
                                                            @if ($errors->has('fin'))

                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('fin') }}</strong>
                                                                </span>

                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class=" col-sm-12 form-group">
                                                        <label>Description de la session (optionelle)</label>
                                                        <textarea name="description" data-provide="markdown" rows="12"
                                                            required>

                                                                            </textarea>
                                                    </div>
                                                    <div class="col-lg-offset-3 col-lg-6 col-sm-12 form-group">
                                                        <div class="col-sm-offset-4 col-sm-5">

                                                            <button class="ladda-button btn btn-sm btn-primary"
                                                                id='ladda-session' data-style="expand-right"
                                                                type="submit">Enregistrer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body" id="tab-fidel">
                                <div class="panel-group" id="accordion">
                                    @forelse ($session as $s)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapse{{ $s->id }}">
                                                        {{ $s->session }} <span
                                                            class="label label-{{ $s->etat == 'active' ? 'success' : 'warning' }}">

                                                            @if ($s->etat == 'active')
                                                                {{ 'En cours' }}
                                                            @else
                                                                @if ($s->etat == 'suspendu')
                                                                    {{ 'En suspend' }}
                                                                @else
                                                                    {{ 'Cloturer' }}
                                                                @endif
                                                            @endif
                                                        </span></a>
                                                </h5>
                                                @if ($s->etat != 'active')
                                                    <div class="ibox-tools">
                                                        <a class="btn btn-default btn-xs btn-rounded" id="ouvrir"
                                                            href=" {{ $s->id }} " tyle="color: #fff">Ouvrir la
                                                            session
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                    </div>

                                                @endif
                                                @if ($s->etat == 'active')
                                                    <div class="ibox-tools">
                                                        <a class="btn btn-info btn-xs btn-rounded" id="suspendre"
                                                            href=" {{ $s->id }} " style="color: #fff">Suspendre la
                                                            session
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <a class="btn btn-danger btn-xs btn-rounded" id="cloturer"
                                                            href=" {{ $s->id }} "
                                                            tyle="color:#FFF !important">Cloturer la session
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                    </div>
                                                @endif

                                            </div>

                                            <div id="collapse{{ $s->id }}"
                                                class="panel-collapse collapse {{ $loop->first ? 'in' : '' }}">
                                                <div class="panel-body">
                                                    <p><b>Date :</b> Du
                                                        {{ \Carbon\Carbon::parse($s->debut)->isoFormat('LL') }}</span>
                                                        au {{ \Carbon\Carbon::parse($s->fin)->isoFormat('LL') }}</p>
                                                    <p>
                                                        <b>
                                                            Description :
                                                        </b>
                                                    </p>
                                                    <p>
                                                        {{ $s->description }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @empty

                                    @endforelse

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('autres_script')
    <script src="{{ asset('js/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
    <script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
    <script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
    {{-- <script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script> --}}
    <script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


    <script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
    <script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

    <script src="{{ asset('js/toastr/toastr.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#formFidel").on("submit", function(e) {
                e.preventDefault();
                add("#formFidel", '#tab-fidel', 'createSessionMeta')
            });
            $(document).on("click", "#suspendre", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // var idv = $(this).attr("title");
                //alert(id);
                modifier(id, '../suspendre');
            });
            $(document).on("click", "#cloturer", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // var idv = $(this).attr("title");
                //alert(id);
                modifier(id, '../suspendreMeta');
            });
            $(document).on("click", "#ouvrir", function(e) {
                e.preventDefault();
                var id = $(this).attr("href");
                // var idv = $(this).attr("title");
                //alert(id);
                modifier(id, '../ouvertureMeta');
            });
        });

        function load(id) {
            $(id).children('.ibox-content').toggleClass('sk-loading');
        }

        function add(form, idLoad, url) {
            var f = form;
            var loade = idLoad;
            var u = url;
            load(loade);
            $.ajax({
                url: u,
                method: "POST",
                data: $(f).serialize(),
                success: function(data) {
                    load(loade);
                    if (!data.reponse) {
                        swal({
                            title: data.msg,
                            icon: 'error'
                        })
                    } else {
                        swal({
                            title: data.msg,
                            icon: 'success'
                        })

                        $(f)[0].reset();
                    }

                },
            });

        }

        function modifier(id, url) {
            swal({
                title: "Attention Modification",
                text: "Cette action modifira l'etat de la session, voulez-vous continuer ?",
                icon: 'warning',
                dangerMode: true,
                buttons: {
                    cancel: 'Non',
                    delete: 'OUI'
                }
            }).then(function(willDelete) {
                if (willDelete) {
                    $.ajax({
                        url: url + "/" + id,
                        method: "GET",
                        data: "",
                        success: function(data) {
                            //  load('#tab-session');
                            if (!data.reponse) {
                                swal({
                                    title: data.msg,
                                    icon: 'error'
                                })

                            } else {
                                swal({
                                    title: data.msg,
                                    icon: 'success'
                                })
                                actualiser();
                            }
                        },
                    });
                } else {
                    swal({
                        title: "Action annuler",
                        icon: 'error'
                    })
                }
            });

        }

        function actualiser() {
            location.reload();
        }
    </script>
@endsection
