@extends('layouts.template',['titre'=>"Smallgroup",'titre2'=>"metamorpho"])

@section('autres_style')
    <link href="{{ asset('css/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/parsley/parsley.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dualListbox/bootstrap-duallistbox.min.css') }}">
    <link href="{{ asset('css/dataTables/datatables.min.css') }}" rel="stylesheet">

@endsection
@section('content')

    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox" id="tab-cvc">
                    <div class="ibox-title">
                        <h5>Ici se fait la création et la gestion des <b>Smallgroup</b> </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tabs-container">
                                    <ul class="nav nav-tabs">
                                        <li class="{{isset($_GET['p'])?'':'active'}}"><a data-toggle="tab" href="#tab-1">Création
                                            </a>
                                        </li>
                                        <li class="{{isset($_GET['p']) && $_GET['p']==1?'active':''}}"><a data-toggle="tab" href="#tab-2">Liste
                                                <span class="label label-danger">{{ $smallFidel->count() }}</span>
                                            </a>
                                        </li>
                                        <li class="{{isset($_GET['p']) && $_GET['p']==2?'active':''}}"><a data-toggle="tab" href="#tab-3">Gestion
                                                <span class="label label-danger">{{ $smallFidel->count() }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab-1" class="tab-pane @if (isset($smalgBy))
                                        {{'active'}}
                                        @else
                                            @if (!isset($_GET['p']))
                                            {{'active'}}
                                            @endif
                                        @endif">
                                            <div class="panel-body" id="tab-fidel">
                                                @if (session()->has('message'))
                                                    <div class="col-md-6 col-md-offset-3">
                                                        <div class="alert alert-success alert-dismissable">
                                                            <button aria-hidden="true" data-dismiss="alert"
                                                                class="close" type="button">×</button>
                                                            {{ session()->get('message') }}
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="ibox-title">
                                                    <h5>Ce formulaire vous permet de créer une session</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                                        <div class="sk-cube1"></div>
                                                        <div class="sk-cube2"></div>
                                                    </div>
                                                    <div class='row'>
                                                        <div class=" col-lg-12 col-sm-12">
                                                            <form
                                                                id="{{ isset($smalgBy) ? 'UpdatSmallgroup' : 'formCreateMentor' }}"
                                                                method="POST" class=""
                                                                action="{{ isset($smalgBy) ? route('updateSmallgroup', ['id' => $smalgBy->id]) : '' }}"
                                                                class='form-group' data-parsley-validate>
                                                                @csrf
                                                                <div class="row">
                                                                    @if (isset($smalgBy))
                                                                        <div>
                                                                            <input name="id" hidden
                                                                                value="{{ $smalgBy->metamorpho_id }}" />
                                                                        </div>
                                                                    @endif
                                                                    <div class="col-sm-4 form-group">
                                                                        <label>Mentore</label>
                                                                        <select class="form-control" name="user_id"
                                                                            id='fidel' required>
                                                                            @if (isset($smalgBy))
                                                                                @forelse ($MentorDispo as $f)
                                                                                    <option value="{{ $f->id }}"
                                                                                        {{ $smalgBy->id == $f->id ? 'selected' : '' }}>
                                                                                        {{ $f->name . '-' . $f->prenom }}
                                                                                    </option>
                                                                                @empty

                                                                                @endforelse
                                                                            @else
                                                                                <option value="" disabled selected>
                                                                                    Choisissez un mentor</option>
                                                                                @forelse ($MentorDispo as $f)
                                                                                    <option value="{{ $f->id }}"
                                                                                        {{ isset($smalgBy->id) ? 'selected' : '' }}>
                                                                                        {{ $f->name . '-' . $f->prenom }}
                                                                                    </option>
                                                                                @empty

                                                                                @endforelse
                                                                            @endif

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <label>Nom du small group</label>
                                                                        <div class="input-control ">
                                                                            <input type="text"
                                                                                placeholder="Nom du small group"
                                                                                value="{{ isset($smalgBy) ? $smalgBy->nom : '' }}"
                                                                                class="form-control" name='nom' required
                                                                                aria-required="true">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <label>Le quota</label>
                                                                        <div class="input-control ">
                                                                            <input type="number" placeholder="Quota"
                                                                                value="{{ isset($smalgBy) ? $smalgBy->quota : '' }}"
                                                                                class="form-control" name='quota' required
                                                                                aria-required="true">
                                                                        </div>
                                                                    </div>

                                                                    <div
                                                                        class="col-lg-offset-3 col-lg-6 col-sm-12 form-group">
                                                                        <div class="col-sm-offset-4 col-sm-5">

                                                                            <button
                                                                                class="ladda-button btn btn-sm btn-primary"
                                                                                id='ladda-session' data-style="expand-right"
                                                                                type="submit">{{ isset($smalgBy) ? 'Modifier' : 'Enregistrer' }}</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-2" class="tab-pane {{isset($_GET['p']) && $_GET['p']==1?'active':''}}">
                                            <div class="panel-body" id="tab-fidel2">
                                                <div class="ibox-content">
                                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                                        <div class="sk-cube1"></div>
                                                        <div class="sk-cube2"></div>
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table
                                                            class="table table-striped table-bordered table-hover dataTables-example">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nom smallgroup</th>
                                                                    <th>Quota</th>
                                                                    <th>Session</th>
                                                                    <th>Mentor</th>
                                                                    <th>Sexe</th>
                                                                    <th>Date de création</th>
                                                                    <th>Options</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($smallFidel as $i)
                                                                    <tr class="gradeX">
                                                                        <td><a href="#contact-{{ $i->id }}"
                                                                                class="client-link">{{ $i->nom }}</a>
                                                                            <span class="label label-success">
                                                                                <strong>{{ $i->fidel->count() }}</span>
                                                                        </td>
                                                                        </td>
                                                                        <td> {{ $i->quota }}

                                                                        <td> {{ $i->metamorpho->session }}</td>
                                                                        <td>{{ $i->user->name . '-' . $i->user->prenom }}
                                                                        </td>
                                                                        <td> {{ $i->sexe == 'H' ? 'HOMME' : 'FEMME' }}</td>
                                                                        <td> {{ \Carbon\Carbon::parse($i->created_at)->isoFormat('LL') }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <a class="btn btn-warning btn-rounded btn-xs"
                                                                                href="{{ route('editeSmallgroup', ['id' => $i->id]) }}"
                                                                                style="margin-bottom:5px">
                                                                                <i class="fa fa-edit"></i> Modifier</a>
                                                                            <a id='deleteSmal' href="{{ $i->id }}"
                                                                                class="btn btn-danger btn-rounded btn-xs">
                                                                                <i class="fa fa-trash-o"></i>Supprimer</a>
                                                                        </td>
                                                                    </tr>

                                                                @empty
                                                                    <div class='wrapper-content  animated fadeInRight'>
                                                                        <div class="row mt-5">
                                                                            <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                                                <p
                                                                                    class="center small text-center  badge badge-danger">
                                                                                    Aucune donnée enregistrer
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforelse

                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Nom smallgroup</th>
                                                                    <th>Quota</th>
                                                                    <th>Session</th>
                                                                    <th>Mentor</th>
                                                                    <th>Sexe</th>
                                                                    <th>Date de création</th>
                                                                    <th>Options</th>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-3" class="tab-pane {{isset($_GET['p']) && $_GET['p']==2?'active':''}}">
                                            <div class="panel-body" id="tab-fidel3">
                                                <div class="ibox-content">
                                                    <div class="sk-spinner sk-spinner-wandering-cubes">
                                                        <div class="sk-cube1"></div>
                                                        <div class="sk-cube2"></div>
                                                    </div>
                                                    <div class='row'>
                                                        <div class=" col-lg-12 col-sm-12">
                                                            <div id="tab-2" class="tab-pane">
                                                                <div class="panel-body">
                                                                    <div class="panel-group" id="accordion">
                                                                        @forelse ($smallFidel as $s)
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <h5 class="panel-title">
                                                                                        <a data-toggle="collapse"
                                                                                            data-parent="#accordion"
                                                                                            href="#collapse{{ $s->id }}">
                                                                                            {{ $s->user->name . ' (' . $s->nom . ')' }}
                                                                                            <span
                                                                                                class="label label-success">{{ $s->fidel->count() }}</span>
                                                                                        </a>
                                                                                        <span
                                                                                            class="badge badge-warning">{{ $s->quota - $s->fidel->count() == 0 ? 'Quota atteind' : 'Reste ' . $s->quota - $s->fidel->count() }}</span>
                                                                                    </h5>
                                                                                    @if ($s->fidel->count() > 0)
                                                                                        <div class="ibox-tools">
                                                                                            <a class=""
                                                                                                id="viderSmall"
                                                                                                href=" {{ $s->id }} "
                                                                                                tyle="color: #FFFFFF !important">
                                                                                                <span
                                                                                                    class="label label-danger">
                                                                                                    <i
                                                                                                        class="fa fa-trash-o"></i>
                                                                                                    Vider le
                                                                                                    smallgroup</span>
                                                                                            </a>
                                                                                            <a class=""
                                                                                                id="sendEmails"
                                                                                                href="{{ $s->id }} "
                                                                                                tyle="color: #FFFFFF !important">
                                                                                                <span
                                                                                                    class="label label-success">
                                                                                                    <i
                                                                                                        class="fa fa-envelope"></i>
                                                                                                    Envoyer des emails</span>
                                                                                            </a>
                                                                                            <a class=""
                                                                                                id="viderSmall"
                                                                                                href=" {{ $s->id }} "
                                                                                                tyle="color: #FFFFFF !important">
                                                                                                <span
                                                                                                    class="label label-primary">
                                                                                                    <i
                                                                                                        class="fa fa-envelope-open-o"></i>
                                                                                                    Envoyer SMS</span>
                                                                                            </a>
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div id="collapse{{ $s->id }}"
                                                                                    class="panel-collapse collapse {{ $loop->first ? 'in' : '' }}">
                                                                                    <div class="panel-body">
                                                                                        <div class="row">
                                                                                            @forelse ($s->fidel as $fidel)
                                                                                                <div class="col-lg-4">
                                                                                                    <div
                                                                                                        class="contact-box center-version">
                                                                                                        <a
                                                                                                            href="{{ route('fideles') }}">
                                                                                                            <img alt="image"
                                                                                                                class="img-circle"
                                                                                                                src="{{ asset('img/default.png') }}">

                                                                                                            <h3
                                                                                                                class="m-b-xs">
                                                                                                                <strong>{{ $fidel->prenom . ' ' . $fidel->nom }}</strong>
                                                                                                            </h3>
                                                                                                            <div
                                                                                                                class="font-bold">
                                                                                                                {{ $fidel->est_de }}
                                                                                                            </div>
                                                                                                            <address
                                                                                                                class="m-t-md">
                                                                                                                <strong>
                                                                                                                    <span
                                                                                                                        class="label label-success">
                                                                                                                        <strong>{{ $fidel->etatCivil }}</span></strong><br>
                                                                                                                {{ $fidel->avecnue . ',' . $fidel->commune . ',' . $fidel->quartier }}<br>
                                                                                                                <abbr
                                                                                                                    title="Phone"><i
                                                                                                                        class="fa fa-phone"></i>
                                                                                                                    :</abbr>
                                                                                                                {{ $fidel->phone }}<br>
                                                                                                                <abbr
                                                                                                                    title="email"><i
                                                                                                                        class="fa fa-envelope"></i>
                                                                                                                    :</abbr>
                                                                                                                {{ $fidel->email }}<br>
                                                                                                            </address> <br>
                                                                                                            <div
                                                                                                                class="clearfix">
                                                                                                                <span
                                                                                                                    class="badge badge-warning">
                                                                                                                    {{ $fidel->baptiser == 'OUI' ? 'déjà baptiser' : 'Pas baptiser' }}
                                                                                                                </span>
                                                                                                                <span
                                                                                                                    class="badge badge-primary">
                                                                                                                    {{ $fidel->etat_activite }}
                                                                                                                </span>
                                                                                                            </div>
                                                                                                        </a>
                                                                                                        <div
                                                                                                            class="contact-box-footer">
                                                                                                            <div
                                                                                                                class="m-t-xs btn-group">
                                                                                                                <a
                                                                                                                    class="btn btn-xs btn-white"><i
                                                                                                                        class="fa fa-envelope-open"></i>
                                                                                                                    SMS </a>
                                                                                                                <a href="{{ $fidel->id }}" id="sendEmail"
                                                                                                                    class="btn btn-xs btn-white"><i
                                                                                                                        class="fa fa-envelope"></i>
                                                                                                                    Email</a>
                                                                                                                <a href="{{ $fidel->id }}"
                                                                                                                    id="detacher"
                                                                                                                    class="btn btn-xs btn-white"><i
                                                                                                                        class="fa fa-trash-o"></i>
                                                                                                                    Détacher</a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @empty
                                                                                                <span
                                                                                                    class="label label-warning">Aucun
                                                                                                    mentore affecté pour le
                                                                                                    moment</span>
                                                                                            @endforelse

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @empty
                                                                            <span class="label label-warning">Aucun
                                                                                smallgroup crée
                                                                                pour le moment</span>
                                                                        @endforelse
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @section('autres_script')
        <script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
        <script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
        {{-- <script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script> --}}
        <script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
        <script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


        <script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
        <script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

        <script src="{{ asset('js/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('js/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>
        <script src="{{ asset('js/dataTables/datatables.min.js') }}"></script>

        <script>
            $(document).ready(function() {

                $('.dataTables-example').DataTable({
                    language: {
                        processing: "Traitement en cours...",
                        search: "Rechercher&nbsp;:",
                        lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                        info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix: "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable: "Aucune donnée disponible dans le tableau",
                        paginate: {
                            first: "Premier",
                            previous: "Pr&eacute;c&eacute;dent",
                            next: "Suivant",
                            last: "Dernier"
                        },
                        aria: {
                            sortAscending: ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    },
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [{
                            extend: 'copy'
                        },
                        {
                            extend: 'csv'
                        },
                        {
                            extend: 'excel',
                            title: 'NewsLetter'
                        },
                        {
                            extend: 'pdf',
                            title: 'NewsLetter'
                        },

                        {
                            extend: 'print',
                            customize: function(win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ]

                });

                $("#formCreateMentor").on("submit", function(e) {
                    e.preventDefault();
                    add("#formCreateMentor", '#tab-fidel', 'createSmllgroup');
                });

                $(document).on("click", "#deleteSmal", function(e) {
                    e.preventDefault();
                    var id = $(this).attr("href");
                    // alert(idv+'-'+id);
                    deleteSmallgroup(id, '../deleteSmallgroup','Supprimer ce group',
                        "Vous êtes sur le point de supprimer ce smallgroup","#tab-fidel2",true,1);
                });
                $(document).on("click", "#viderSmall", function(e) {
                    e.preventDefault();
                    var id = $(this).attr("href");
                    // alert(idv+'-'+id);
                    deleteSmallgroup(id, '../viderSmallgroup','Vider ce group',
                        "Vous êtes sur le point de vidé ce smallgroup de ses mentores","#tab-fidel3",true,2);
                });
                $(document).on("click", "#sendEmails", function(e) {
                    e.preventDefault();
                    var id = $(this).attr("href");
                    // alert(idv+'-'+id);
                    deleteSmallgroup(id, '../sendEmails','Envoi email à un group',
                        "Vous êtes sur le point d\'envoyer un email aux membres de ce smallgroup",'#tab-fidel3',false,"");
                });
                $(document).on("click", "#sendEmail", function(e) {
                    e.preventDefault();
                    var id = $(this).attr("href");
                    // alert(idv+'-'+id);
                    deleteSmallgroup(id, '../sendEmail','Envoi email personel',
                        "Vous êtes sur le point d\'envoyer un email à ce mentore",'#tab-fidel3',false,0);
                });
                $(document).on("click", "#detacher", function(e) {
                    e.preventDefault();
                    var id = $(this).attr("href");
                    // alert(idv+'-'+id);
                    deleteSmallgroup(id, '../detacheMentore','Attention Détachement',
                        "Vous êtes sur le point de Détacher ce mentore du smallgroup!!",'#tab-fidel3',true,2);
                });
            });

            function load(id) {
                $(id).children('.ibox-content').toggleClass('sk-loading');
            }

            function add(form, idLoad, url) {
                var f = form;
                var loade = idLoad;
                var u = url;
                load(loade);
                $.ajax({
                    url: u,
                    method: "POST",
                    data: $(f).serialize(),
                    success: function(data) {
                        load(loade);
                        if (!data.reponse) {
                            swal({
                                title: data.msg,
                                icon: 'error'
                            })
                        } else {
                            swal({
                                title: data.msg,
                                icon: 'success'
                            })
                            $('#fidel').val('');
                            $(f)[0].reset();
                        }

                    },
                });

            }

            function deleteSmallgroup(id, url,titre, msg,loade,actual,p) {
                swal({
                    title: titre,
                    text: msg,
                    icon: 'warning',
                    dangerMode: true,
                    buttons: {
                        cancel: 'Non',
                        delete: 'OUI'
                    }
                }).then(function(willDelete) {
                    if (willDelete) {
                        load(loade);
                        $.ajax({
                            url: url + "/" + id,
                            method: "GET",
                            data: "",
                            success: function(data) {
                                load(loade);
                                if (!data.reponse) {
                                    swal({
                                        title: data.msg,
                                        icon: 'error'
                                    })

                                } else {
                                    swal({
                                        title: data.msg,
                                        icon: 'success'
                                    })
                                    if(actual==true){
                                        location.replace('addSmallgroup?p='+p);
                                        // actualiser();
                                    }
                                }
                            },
                        });
                    } else {
                        swal({
                            title: "Action annuler",
                            icon: 'error'
                        })
                    }
                });
            }

            function actualiser() {
                location.reload();
            }
        </script>
    @endsection
