@extends('layouts.template',['titre'=>"Page rapport",'titre2'=>"Page rapport"])

@section('autres_style')
<link href="{{ asset('css/bootstrap-markdown/bootstrap-markdown.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="panel-body" id="tab-fidel">
            @if (session()->has('message'))
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert"
                            class="close" type="button">×</button>
                        {{ session()->get('message') }}
                    </div>
                </div>
            @endif
            <div class="ibox-title">
                <h5>Ce formulaire vous permet de créer une session</h5>
            </div>
            <div class="ibox-content">
                <div class="sk-spinner sk-spinner-wandering-cubes">
                    <div class="sk-cube1"></div>
                    <div class="sk-cube2"></div>
                </div>
                <div class='row'>
                    <div class=" col-lg-12 col-sm-12">
                        <form
                            id="{{ isset($smalgBy) ? 'UpdatSmallgroup' : 'formRapport' }}"
                            method="POST" class=""
                            action="{{ isset($smalgBy) ? route('updateSmallgroup', ['id' => $smalgBy->id]) : '' }}"
                            class='form-group' data-parsley-validate>
                            @csrf
                            <div class="row">
                                @if (isset($smalgBy))
                                    <div>
                                        <input name="id" hidden
                                            value="{{ $smalgBy->metamorpho_id }}" />
                                    </div>
                                @endif
                                <div class="col-sm-12 form-group">
                                    <label>Mentore</label>
                                    <select class="form-control" name="fidel_id"
                                        id='fidel' required>
                                            <option value="" disabled selected>
                                                Choisissez un mentore</option>
                                            @forelse ($MySmallgroup as $f)
                                            @foreach ($f->fidel as $ff)                                  
                                            @foreach ($ff->metamorpho as $fff)              
                                                             
                                                @if ($ff->est_de=='Phila'&& $ff->est_de=='Phila'&& $fff->pivot->etat=='0')                                                    
                                                <option value="{{ $ff->id }}"
                                                    {{ isset($smalgBy->id) ? 'selected' : '' }}>
                                                    {{ $ff->nom . '-' . $ff->prenom}}
                                                </option>
                                                @endif
                                                @endforeach                                                
                                                @endforeach                                                
                                            @empty
                                            @endforelse

                                    </select>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label>Debut</label>
                                    <select name="debut" id="" class="form-control">
                                        <option value="Bon">BON</option>
                                        <option value="Reserver">RESERVER</option>
                                        <option value="Bien">BIEN</option>
                                    </select>
                                </div>
                                <div class="col-sm-8 form-group">
                                    <label>Commentaire</label>
                                    <div class="input-control ">
                                        <input type="text"
                                            value="{{ isset($smalgBy) ? $smalgBy->nom : '' }}"
                                            class="form-control" name='comment_debut' required
                                            aria-required="true">
                                    </div>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label>Fin</label>
                                    <select name="fin" id=""  class="form-control">
                                        <option value="Bon">BON</option>
                                        <option value="Reserver">RESERVER</option>
                                        <option value="Bien">BIEN</option>
                                    </select>
                                </div>
                                <div class="col-sm-8 form-group">
                                    <label>Commentaire</label>
                                    <div class="input-control ">
                                        <input type="text"
                                            value="{{ isset($smalgBy) ? $smalgBy->nom : '' }}"
                                            class="form-control" name='comment_fin' required
                                            aria-required="true">
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Avis</label>
                                    <div class="input-control ">
                                        <textarea  name="avis" data-provide="markdown" rows="10">
                                             </textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Observation</label>
                                    <div class="input-control ">
                                        <textarea  name="observation" data-provide="markdown" rows="10">
                                          </textarea>
                                    </div>
                                </div>

                                <div
                                    class="col-lg-offset-3 col-lg-6 col-sm-12 form-group">
                                    <div class="col-sm-offset-4 col-sm-5">
                                        <button
                                            class="ladda-button btn btn-sm btn-primary"
                                            id='ladda-session' data-style="expand-right"
                                            type="submit">{{ isset($smalgBy) ? 'Modifier' : 'Enregistrer' }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('autres_script')
<script src="{{ asset('js/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#formRapport").on("submit", function (e) {
            e.preventDefault();
             add("#formRapport", '#tab-fidel', 'sendRapport')
        });
    });

    function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
    }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })
                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })

                    $(f)[0].reset();
                }

            },
        });

    }
function actualiser() {
    location.reload();
}

</script>

@endsection
