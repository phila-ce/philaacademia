@extends('layouts.template',['titre'=>"Fidèles",'titre2'=>"Fidèles"])

@section('autres_style')
<link href="{{ asset('css/dataTables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/footable/footable.core.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                @if(session()->has('message'))
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {{session()->get('message')}}
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="col-sm-9">
            <div class="ibox" >
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wandering-cubes">
                        <div class="sk-cube1"></div>
                        <div class="sk-cube2"></div>
                    </div>
                    <span class="text-muted small pull-right">Last modification: <i class="fa fa-clock-o"></i> 2:10 pm -
                        12.06.2014</span>
                    <h2>Fidèles</h2>
                    <p>
                       Cette page vous permet de gerer les fidèles
                    </p>
                    <div class="input-control">
                        <input type="text" placeholder="Faites la recherche d'un fidèle"
                            class="form-control input-sm m-b-x" id="filter">

                    </div>
                    <div class="clients-list">
                        <ul class="nav nav-tabs">
                            {{-- <span class="pull-right small text-muted">{{ $fidels->count().'Fidels' }}</span> --}}
                            <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-user"></i>En
                                    cours<span class="badge badge-success">{{ $fidels->count() }}</span>
                           </a></li>
                           <li class=""><a data-toggle="tab" href="#tab-5"><i class="fa fa-user"></i>Extern
                            <span class="badge badge-success">{{ $fidelsExterieur->count() }}</span></a>
                        </li>
                            <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-briefcase"></i>Fini
                                <span class="badge badge-success">{{ $fidelsFin->count() }}</span></a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3"><i class="fa fa-briefcase"></i>All
                                <span class="badge badge-success">{{ $fidelexport->count() }}</span></a>
                            </li>
                           
                            <li class=""><a data-toggle="tab" href="#tab-4"><i class="fa fa-lock"></i>Suspendu
                                <span class="badge badge-success">{{ $fidelsSuspendu->count() }}</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="full-height-scroll">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover footable"
                                            data-page-size="1000" data-filter=#filter>
                                            <tbody>
                                                @forelse ($fidels as $i)
                                                <tr>
                                                    <td class="client-avatar"><img alt="image"
                                                            src="{{ asset('img/default.png') }}"> </td>
                                                    <td><a data-toggle="tab" href="#contact-{{ $i->id }}"
                                                            class="client-link">{{ $i->prenom.'-'.$i->nom }}</a></td>
                                                    <td> {{ $i->phone }}</td>
                                                    <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                                    <td> {{ $i->email }}</td>
                                                    <td class="client-status"><span
                                                            class="label label-{{ $i->etat_activite==='enregistrer'?'warning':'primary' }}">{{ $i->etat_activite }}</span>
                                                    </td>
                                                </tr> 
                                                @empty
                                                <div class='wrapper-content  animated fadeInRight'>
                                                    <div class="row mt-5">
                                                        <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                            <p class="center small text-center  badge badge-danger">
                                                                Aucun fidèle enregistrer
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-5" class="tab-pane">
                                <div class="full-height-scroll">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover footable"
                                            data-page-size="1000" data-filter=#filter>
                                            <tbody>
                                                @forelse ($fidelsExterieur as $i)
                                                <tr>
                                                    <td class="client-avatar"><img alt="image"
                                                            src="{{ asset('img/default.png') }}"> </td>
                                                    <td><a data-toggle="tab" href="#contact-{{ $i->id }}"
                                                            class="client-link">{{ $i->prenom.'-'.$i->nom }}</a></td>
                                                    <td> {{ $i->phone }}</td>
                                                    <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                                    <td> {{ $i->email }}</td>
                                                    <td class="client-status"><span
                                                            class="label label-{{ $i->etat_activite==='enregistrer'?'warning':'primary' }}">{{ $i->etat_activite }}</span>
                                                    </td>
                                                </tr> 
                                                @empty
                                                <div class='wrapper-content  animated fadeInRight'>
                                                    <div class="row mt-5">
                                                        <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                            <p class="center small text-center  badge badge-danger">
                                                                Aucun fidèle enregistrer
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="full-height-scroll">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <tbody>
                                                @forelse ($fidelsFin as $i)
                                                <tr>
                                                    <td class="client-avatar"><img alt="image"
                                                            src="{{ asset('img/default.png') }}"> </td>
                                                    <td><a data-toggle="tab" href="#contact-{{ $i->id }}"
                                                            class="client-link">{{ $i->prenom.'-'.$i->nom }}</a></td>
                                                    <td> {{ $i->phone }}</td>
                                                    <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                                    <td> gravida@rbisit.com</td>
                                                    <td class="client-status"><span
                                                            class="label label-success">{{ $i->etat_activite }}</span>
                                                    </td>
                                                </tr>

                                                @empty
                                                <div class='wrapper-content  animated fadeInRight'>
                                                    <div class="row mt-5">
                                                        <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                            <p class="center small text-center  badge badge-danger">
                                                                Aucun fidèle enregistrer
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="full-height-scroll">
                                    <div class="table-responsive">
                                        <table
                                            class="table table-striped table-bordered table-hover dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Nom-Prenom</th>
                                                    <th>Sexe</th>
                                                    <th>Telephone</th>
                                                    <th>Etat civil</th>
                                                    <th>Commune</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($fidelexport as $i)
                                                <tr class="gradeX">
                                                    <td><a data-toggle="tab" href="#contact-{{ $i->id }}"
                                                            class="client-link">{{ $i->prenom.'-'.$i->nom }}</a></td>
                                                    <td> {{ $i->sexe }}</td>
                                                    <td> {{ $i->phone }}</td>
                                                    <td>{{ $i->etatCivil }}</td>
                                                    <td>{{ $i->commune }}</td>
                                                </tr>

                                                @empty
                                                <div class='wrapper-content  animated fadeInRight'>
                                                    <div class="row mt-5">
                                                        <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                            <p class="center small text-center  badge badge-danger">
                                                                Aucun fidèle enregistrer
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforelse

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Nom-Prenom</th>
                                                    <th>Sexe</th>
                                                    <th>Telephone</th>
                                                    <th>Etat civil</th>
                                                    <th>Commune</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane">
                                <div class="full-height-scroll">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <tbody>
                                                @forelse ($fidelsSuspendu as $i)
                                                <tr>
                                                    <td class="client-avatar"><img alt="image"
                                                            src="{{ asset('img/default.png') }}"> </td>
                                                    <td><a data-toggle="tab" href="#contact-{{ $i->id }}"
                                                            class="client-link">{{ $i->prenom.'-'.$i->nom }}</a></td>
                                                    <td> {{ $i->phone }}</td>
                                                    <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                                    <td> gravida@rbisit.com</td>
                                                    <td class="client-status"><span
                                                            class="label label-danger">{{ $i->etat_activite }}</span>
                                                    </td>
                                                </tr>

                                                @empty
                                                <div class='wrapper-content  animated fadeInRight'>
                                                    <div class="row mt-5">
                                                        <div class='col-lg-6 col-md-push-4 col-sm-12'>
                                                            <p class="center small text-center  badge badge-danger">
                                                                Aucun fidèle Suspendu
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3" >
            <div class="ibox " id="tab-fidel">

                <div class="ibox-content">
                    <div class="tab-content">
                        @forelse ($fidelexport as $f)

                        <div id="contact-{{ $f->id }}" class="tab-pane ">
                            <div class="row m-b-lg">
                                <div class="col-lg-12 text-center">
                                    <h2>{{ $f->prenom.'-'.$f->nom}}</h2>

                                    <div class="m-b-sm">
                                        <img alt="image" class="img-circle" src="{{ asset('img/default.png') }}"
                                            style="width: 62px">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <a href="{{ route('fidelBy', ['id'=> $f->id]) }}" class="btn btn-outline btn-success btn-sm btn-block" style="margin-bottom: 10px">Voir plus</a>
                                    @if ($f->etat_activite==='enregistrer')
                                    <form id="suspenduForme" action="{{ route('suspendFidel') }}" method="POST">
                                        @csrf
                                        <input hidden type="text" name="mode" value="suspendu"/>
                                        <input hidden type="text" id='fidel_id{{ $f->id }}' name="fidel_id" value="{{ $f->id }}"/>
                                        <button type="submit" id='btnSuspFidel' class="btn btn-danger btn-sm btn-block"><i
                                                class="fa fa-lock"></i> Suspendre ce fidèle
                                        </button>
                                    </form>
                                    <form id="suspenduForme" action="{{ route('debutCursus') }}" method="POST" style="margin-top: 10px">
                                        @csrf
                                        <input hidden type="text" name="mode" value="active"/>
                                        <input hidden type="text" id='fidel_id{{ $f->id }}' name="fidel_id" value="{{ $f->id }}"/>
                                        
                                        <button id='btnDebutCursuse' type="submit" class="btn btn-outline btn-success btn-sm btn-block"><i
                                                class="fa fa-check"></i> Démarer le cursus
                                        </button>
                                    </form>
                                    @elseif ($f->etat_activite==='active')
                                    <form id="suspenduForme" action="{{ route('suspendFidel') }}" method="POST">
                                        @csrf
                                        <input hidden type="text" name="mode" value="suspendu"/>
                                        <input hidden type="text" id='fidel_id{{ $f->id }}' name="fidel_id" value="{{ $f->id }}"/>
                                       <button  class="btn btn-primary btn-sm btn-block"><i
                                                class="fa fa-lock"></i> Bloquer ce fidèle
                                        </button>
                                    </form>
                                        @else
                                        @if ($f->etat_activite==='fini')
                                        <a href='{{ $f->id }}' class="btn btn-success btn-sm btn-block"><i
                                                class="fa fa-folder"></i> Cursus fini
                                        </a>
                                        @else
                                            <form id="suspenduForme" action="{{ route('debloquerFidel') }}" method="POST">
                                            @csrf
                                            <input hidden type="text" id='fidel_id{{ $f->id }}' name="fidel_id" value="{{ $f->id }}"/>
                                                <button href='{{ $f->id }}' class="btn btn-primary btn-sm btn-block"><i
                                                    type='submit'  id="debloquerFidel"  class="fa fa-unlock"></i> Débloquer le fidèle
                                                </button>
                                        </form>
                                        @endif


                                        @endif

                                </div>
                            </div>
                            <div class="client-detail">
                                <div class="full-height-scroll">

                                    <strong>Identité en detail</strong>

                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">{{ $f->sexe }} </span>
                                            Sexe
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{ $f->lieu}}</span>
                                            Lieu :
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{ $f->datenaissance}} </span>
                                            Date de naissance :
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{ $f->phone}} </span>
                                            Telephone :
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{ $f->email}}</span>
                                            Email :
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{ $f->etatCivil}} </span>
                                            Etat Civil :
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{ $f->baptiser?'OUI':'NON'}} </span>
                                            Baptiser :
                                        </li>
                                    </ul>
                                    <strong>Légende des couleurs :</strong>
                                    <p>
                                        <div>
                                            <table class="table">

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-xs btn-danger m-r-sm">CVC</button>
                                                            Pas encore commencer
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-xs btn-warning m-r-sm">Meta</button>
                                                            En cour
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-xs btn-success m-r-sm">ECAP</button>
                                                            Fini
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-danger mr-10">
                                                                CVC :
                                                            </span>
                                                        <strong>
                                                            Cultivé la vie de christ
                                                        </strong>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-danger">
                                                                Meta :
                                                            </span>

                                                          <strong>
                                                            Métamorpho
                                                        </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-danger">
                                                                Ecap :
                                                            </span>

                                                           <strong>
                                                            Ecole d'apolos
                                                        </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-danger">
                                                                G :
                                                            </span>
                                                            <strong>
                                                                Gifted
                                                            </strong>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </p>
                                    <hr />
                                    <strong>Cursus</strong>
                                    <div id="vertical-timeline" class="vertical-container dark-timeline">
                                        @if (!$f->cultives->isEmpty())
                                        @foreach ($f->cultives as $c)
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon @if($c->pivot->etat==1)
                                                {{ 'bg-success' }}
                                                @else
                                                    {{ 'bg-warning'}}
                                                    @endif">
                                                <small>CVC</small>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    @if ($c->pivot->etat==1)
                                                        {{ 'Cursus terminer' }}
                                                    @else
                                                    {{ 'Cursus en cours' }}
                                                    @endif
                                                </p>
                                                <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="vertical-timeline-icon @if($f->etat_activite=='fini')
                                            {{ 'bg-success' }}
                                            @else
                                                {{ 'bg-danger'}}
                                                @endif">
                                            <small>CVC</small>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <p>
                                                @if ($f->etat_activite=='fini')
                                                    {{ 'Le fidèle a déjà fini ce cursus '}}
                                                @else
                                                    {{ 'Le fidèle n\'a pas encore debuté' }}
                                                @endif
                                            </p>
                                            <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014
                                            </span>
                                        </div>
                                        @endif
                                        @if (!$f->metamorpho->isEmpty())
                                        @foreach ($f->metamorpho as $c)
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon @if($c->pivot->etat==1)
                                                {{ 'bg-success' }}
                                                @else
                                                    {{ 'bg-warning'}}
                                                    @endif">
                                                <small>Meta</small>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    @if ($c->pivot->etat==1)
                                                        {{ 'Cursus terminer' }}
                                                    @else
                                                    {{ 'Cursus en cours' }}
                                                    @endif
                                                </p>
                                                <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon @if($f->etat_activite=='fini')
                                            {{ 'bg-success' }}
                                            @else
                                                {{ 'bg-danger'}}
                                                @endif">
                                            <small>Meta</small>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <p>
                                                @if ($f->etat_activite=='fini')
                                                    {{ 'Le fidèle a déjà fini ce cursus '}}
                                                @else
                                                    {{ 'Le fidèle n\'a pas encore debuté' }}
                                                @endif
                                            </p>
                                            <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014
                                            </span>
                                        </div>
                                        </div>
                                        @endif

                                        @if (!$f->ecap->isEmpty())
                                        @foreach ($f->ecap as $c)
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon @if($c->pivot->etat==1)
                                                {{ 'bg-success' }}
                                                @else
                                                    {{ 'bg-warning'}}
                                                    @endif">
                                                <small>Ecap</small>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    @if ($c->pivot->etat==1)
                                                        {{ 'Cursus terminer' }}
                                                    @else
                                                    {{ 'Cursus en cours' }}
                                                    @endif
                                                </p>
                                                <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon @if($f->etat_activite=='fini')
                                            {{ 'bg-success' }}
                                            @else
                                                {{ 'bg-danger'}}
                                                @endif">
                                            <small>Ecap</small>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <p>
                                                @if ($f->etat_activite=='fini')
                                                    {{ 'Le fidèle a déjà fini ce cursus '}}
                                                @else
                                                    {{ 'Le fidèle n\'a pas encore debuté' }}
                                                @endif
                                            </p>
                                            <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014
                                            </span>
                                        </div>
                                        </div>
                                        @endif
                                        @if (!$f->gifted->isEmpty())
                                        @foreach ($f->gifted as $c)
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon @if($c->pivot->etat==1)
                                                {{ 'bg-success' }}
                                                @else
                                                    {{ 'bg-warning'}}
                                                    @endif">
                                                <small>G</small>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>
                                                    @if ($c->pivot->etat==1)
                                                        {{ 'Cursus terminer' }}
                                                    @else
                                                    {{ 'Cursus en cours' }}
                                                    @endif
                                                </p>
                                                <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014
                                                </span>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon @if($f->etat_activite=='fini')
                                            {{ 'bg-success' }}
                                            @else
                                                {{ 'bg-danger'}}
                                                @endif">
                                            <small>G</small>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <p>
                                                @if ($f->etat_activite=='fini')
                                                    {{ 'Le fidèle a déjà fini ce cursus '}}
                                                @else
                                                    {{ 'Le fidèle n\'a pas encore debuté' }}
                                                @endif
                                            </p>
                                            <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014
                                            </span>
                                        </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        @empty

                        @endforelse
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('autres_script')
<script src="{{asset('js/dataTables/datatables.min.js')}}"></script>
<script src="{{asset('js/footable/footable.all.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.footable').footable();
        $('.dataTables-example').DataTable({
            language: {
                processing: "Traitement en cours...",
                search: "Rechercher&nbsp;:",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible dans le tableau",
                paginate: {
                    first: "Premier",
                    previous: "Pr&eacute;c&eacute;dent",
                    next: "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            },
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'NewsLetter'
                },
                {
                    extend: 'pdf',
                    title: 'NewsLetter'
                },

                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

        $("#btnDebutCursus").on("click", function (e) {
            e.preventDefault();
            id=$(this).attr('href');
             // alert(id);
             add("#suspenduForme", '#tab-fidel', 'active')
        });
        $("#debloquerFidel").on("click", function (e) {
            e.preventDefault();
            id=$(this).attr('href');
             // alert(id);
           //  add("#suspenduForme", '#tab-fidel', 'suspendFidel')
        });
        $("#btnSuspFidel").on("click", function (e) {
            e.preventDefault();
            id=$('#fidel_id').val();
             // alert(id);
             add("#suspenduForme", '#tab-fidel', 'suspendFidel')
        });
    });

    function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
    }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })
                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })

                    $(f)[0].reset();
                }

            },
        });

    }
    function bloquerFidel(id,loade, url) {

swal({
    title: "Attention Suspension",
    text: "Vous êtes sur le pont de suspendre ce fidèle, êtes vous sûre de le faire?",
    icon: 'warning',
    dangerMode: true,
    buttons: {
        cancel: 'Non',
        delete: 'OUI'
    }
}).then(function (willDelete) {
    if (willDelete) {
        load(loade);
        $.ajax({
            url: url + "/" + id,
            method: "GET",
            data: "",
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })

                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })
                    actualiser();
                }
            },
        });
    } else {
        swal({
            title: "Suppression annuler",
            icon: 'error'
        })
    }
});
}
function actualiser() {
    location.reload();
}

</script>

@endsection
