@extends('layouts.template',['titre'=>"Début-metamorpho",'titre2'=>"CVC"])


@section('autres_style')
<link href="{{asset('css/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('js/parsley/parsley.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/dualListbox/bootstrap-duallistbox.min.css') }}">

@endsection
@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox" id="tab-cvc">
                <div class="ibox-title">
                    <h5>Début metamorpho  <b>(Meta)</b> </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li class="{{ $title=="Début-CVC"?"active":"" }}"><a href="{{ route('g_cultive') }}">Début CVC</a>
                            </li>
                            <li class="{{ $title=="Début-metamorpho"?"active":"" }}"><a href="{{ route('debutMeta') }}">Envoyer à metamorpho</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wandering-cubes">
                        <div class="sk-cube1"></div>
                        <div class="sk-cube2"></div>
                    </div>
                    <p>
                       Selectionez au moin un fidèle et validé afin qu'il soit considerer comme inscrit dans métamorpho pour la
                       {{-- <b>{{  $sessionActive->isNotEmpty()?$sessionActive->session:''}}</b> --}}
                        qui est ouverte.
                    </p>

                    <form id="debutMetaForm" action="#" class="wizard-big">
                        @csrf
                        <select class="form-control dual_select" multiple name="fidel_id" id='fidel'>
                            @forelse ($fidels as $f)
                            {{-- @forelse ($f ->cultives as $ff)
                            @if ($ff->pivot->etat==1)                                 --}}
                            <option value="{{$f->id}}">{{ $f->nom.'-'.$f->prenom.' ( '.$f->phone.')' }}</option>
                            {{-- @endif
                            @empty
                                
                            @endforelse --}}
                           
                            @empty

                            @endforelse
                        </select>
                        <input name="tabFidel" id="inpute" hidden/>

                        <button class="ladda-button btn btn-sm btn-primary"
                        id='ladda-session' data-style="expand-left"
                        type="submit">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('autres_script')
<script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
<script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


<script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
<script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

<script src="{{ asset('js/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('js/dualListbox/jquery.bootstrap-duallistbox.js') }}"></script>

<script>
        $(document).ready(function () {
            $("#formFidel").on("submit", function (e) {
            e.preventDefault();

            add("#formFidel", '#tab-fidel', 'debutCvc');
        });
        $('#fidel').change(function(){
            $('#inpute').val($(this).val());
        });
            $("#debutMetaForm").on("submit", function (e) {
            e.preventDefault();
            if ($('#fidel').val()!='') {
               add("#debutMetaForm", '#tab-cvc', 'debutMetaAffectation')
            } else {
                swal({
                        title: 'Veillez selectionnez au-moins un fidèle!',
                        icon: 'error'
                    })
            }
        });

        $('.dual_select').bootstrapDualListbox({
                selectorMinimalHeight: 160
            });
        });
 function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
    }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })
                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })
                    $('#fidel').val('');
                    $(f)[0].reset();
                }

            },
        });

    }


</script>
@endsection
