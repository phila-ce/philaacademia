@extends('layouts.template',['titre'=>"Cultivé",'titre2'=>"CVC"])


@section('autres_style')
<link href="{{asset('css/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('js/parsley/parsley.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/iCheck/custom.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-markdown/bootstrap-markdown.min.css') }}">

@endsection
@section('content')

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="panel-body" id="tab-fidel">
                            <div class="ibox-title">
                                <h5>Ce formulaire vous permet de créer une session</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wandering-cubes">
                                    <div class="sk-cube1"></div>
                                    <div class="sk-cube2"></div>
                                </div>
                                <div class='row'>
                                    <div class=" col-lg-12 col-sm-12">
                                        <form id="formFidel" method="POST" class="" action="" class='form-group'  data-parsley-validate>
                                            @csrf
                                            <div class="row">
                                                <div>
                                                    <input name="id" hidden value=""/>
                                                </div>
                                                <div class="col-sm-4 form-group ">
                                                    <label>Session</label>
                                                    <input type="text" placeholder="Entrez le Nom " class="form-control"
                                                        name='session' required aria-required="true" value=""
                                                        data-parsley-minlength="2" data-parsley-trigger="change">
                                                    @if ($errors->has('session'))

                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('session')}}</strong>
                                                    </span>

                                                    @endif
                                                </div>
                                                <div class="col-sm-4 form-group">
                                                    <label>Date de debut</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i
                                                                class="fa fa-calendar"></i></span>
                                                        <input type="date" placeholder="Date de debut" class="form-control"
                                                            data-parsley-trigger="change" data-parsley-trigger="change"  name='debut'
                                                            required aria-required="true" >
                                                        @if ($errors->has('debut'))

                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('debut')}}</strong>
                                                        </span>

                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 form-group">
                                                    <label>Date de Fin</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i
                                                                class="fa fa-calendar"></i></span>
                                                        <input type="date" placeholder="Date de Fin" class="form-control"
                                                            data-parsley-trigger="change" data-parsley-trigger="change"  name='fin'
                                                            required aria-required="true" >
                                                        @if ($errors->has('fin'))

                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('fin')}}</strong>
                                                        </span>

                                                        @endif
                                                    </div>
                                                </div>
                                                <div class=" col-sm-12 form-group">
                                                    <label>Description de la session (optionelle)</label>
                                                    <textarea name="description" data-provide="markdown" rows="12" required>

                                                    </textarea>
                                                </div>
                                                <div class="col-lg-offset-3 col-lg-6 col-sm-12 form-group">
                                                    <div class="col-sm-offset-4 col-sm-5">

                                                        <button class="ladda-button btn btn-sm btn-primary"
                                                            id='ladda-session' data-style="expand-right"
                                                            type="submit">Enregistrer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('autres_script')
<script src="{{ asset('js/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('js/bootstrap-markdown/markdown.js') }}"></script>
<script src="{{ asset('js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


<script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
<script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

<script src="{{ asset('js/toastr/toastr.min.js') }}"></script>

<script>
        $(document).ready(function () {
            $("#formFidel").on("submit", function (e) {
            e.preventDefault();
            add("#formFidel", '#tab-fidel', 'createSessionCVC')
        });
        });
 function load(id) {
        $(id).children('.ibox-content').toggleClass('sk-loading');
    }

    function add(form, idLoad, url) {
        var f = form;
        var loade = idLoad;
        var u = url;
        load(loade);
        $.ajax({
            url: u,
            method: "POST",
            data: $(f).serialize(),
            success: function (data) {
                load(loade);
                if (!data.reponse) {
                    swal({
                        title: data.msg,
                        icon: 'error'
                    })
                } else {
                    swal({
                        title: data.msg,
                        icon: 'success'
                    })

                    $(f)[0].reset();
                }

            },
        });

    }


</script>
@endsection
