@extends('layouts.template',['titre'=>"fidels",'titre2'=>"fidel"])


@section('autres_style')

@endsection
@section('content')

    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row m-b-lg m-t-lg">
            <div class="col-md-6">

                <div class="profile-image">
                    <img src="{{ asset('img/default.png') }}" class="img-circle circle-border m-b-md" alt="profile">
                </div>
                <div class="profile-info">
                    <div class="">
                        <div>
                            <h2 class="no-margins">
                                {{ $fidel->prenom . ' ' . $fidel->nom }}
                            </h2>
                            <h4>{{ $fidel->sexe }}</h4>
                            <small>
                                There are many variations of passages of Lorem Ipsum available, but the majority
                                have suffered alteration in some form Ipsum available.
                            </small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <table class="table small m-b-xs">
                    <span>Legende des couleurs :</span>
                    <tbody>
                        <tr>
                            <td>
                                <strong>
                                    <button type="button" class="btn btn-xs btn-danger m-r-sm">CVC</button>
                                </strong>
                            </td>
                            <td>
                                <strong>Pas encore commencer</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <button type="button" class="btn btn-xs btn-warning m-r-sm">Meta</button>
                                </strong>
                            </td>
                            <td>
                                <strong> En cour</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <button type="button" class="btn btn-xs btn-success m-r-sm">ECAP</button>
                                </strong>
                            </td>
                            <td>
                                <strong>Fini</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <small>Sales in last 24h</small>
                <h2 class="no-margins">206 480</h2>
                <div id="sparkline1"></div>
            </div>


        </div>
        <div class="row">

            <div class="col-lg-3">

                <div class="ibox">
                    <div class="ibox-content">
                        <h3>About Alex Smith</h3>

                        <p class="small">
                            There are many variations of passages of Lorem Ipsum available, but the majority have
                            suffered alteration in some form, by injected humour, or randomised words which don't.
                            <br />
                            <br />
                            If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't
                            anything embarrassing
                        </p>

                        <p class="small font-bold">
                            <span><i class="fa fa-circle text-navy"></i> Online status</span>
                        </p>

                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Followers and friends</h3>
                        <p class="small">
                            If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't
                            anything embarrassing
                        </p>
                        <div class="user-friends">
                            <a href=""><img alt="image" class="img-circle" src="img/a3.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a1.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a2.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a4.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a5.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a6.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a7.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a8.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a2.jpg"></a>
                            <a href=""><img alt="image" class="img-circle" src="img/a1.jpg"></a>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Personal friends</h3>
                        <ul class="list-unstyled file-list">
                            <li><a href=""><i class="fa fa-file"></i> Project_document.docx</a></li>
                            <li><a href=""><i class="fa fa-file-picture-o"></i> Logo_zender_company.jpg</a></li>
                            <li><a href=""><i class="fa fa-stack-exchange"></i> Email_from_Alex.mln</a></li>
                            <li><a href=""><i class="fa fa-file"></i> Contract_20_11_2014.docx</a></li>
                            <li><a href=""><i class="fa fa-file-powerpoint-o"></i> Presentation.pptx</a></li>
                            <li><a href=""><i class="fa fa-file"></i> 10_08_2015.docx</a></li>
                        </ul>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Private message</h3>

                        <p class="small">
                            Send private message to Alex Smith
                        </p>

                        <div class="form-group">
                            <label>Subject</label>
                            <input type="email" class="form-control" placeholder="Message subject">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" placeholder="Your message" rows="3"></textarea>
                        </div>
                        <button class="btn btn-primary btn-block">Send</button>

                    </div>
                </div>

            </div>

            <div class="col-lg-5">

                <div class="social-feed-box">

                    <div class="pull-right social-action dropdown">
                        <button data-toggle="dropdown" class="dropdown-toggle btn-white">
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu m-t-xs">
                            <li><a href="#">Config</a></li>
                        </ul>
                    </div>
                    <div class="social-avatar">
                        <a href="" class="pull-left">
                            <img alt="image" src="img/a1.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                {{$fidel->sexe}}
                            </a>
                            <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                        </div>
                    </div>
                    <div class="social-body">
                        <p>
                            Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                            default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Packages and web page editors now use Lorem Ipsum as their
                            default model text.
                        </p>

                        <div class="btn-group">
                            <button class="btn btn-white btn-xs"><i class="fa fa-thumbs-up"></i> Like this!</button>
                            <button class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comment</button>
                            <button class="btn btn-white btn-xs"><i class="fa fa-share"></i> Share</button>
                        </div>
                    </div>
                    <div class="social-footer">
                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a1.jpg">
                            </a>
                            <div class="media-body">
                                <a href="#">
                                    Andrew Williams
                                </a>
                                Internet tend to repeat predefined chunks as necessary, making this the first true generator
                                on the Internet. It uses a dictionary of over 200 Latin words.
                                <br />
                                <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 26 Like this!</a> -
                                <small class="text-muted">12.06.2014</small>
                            </div>
                        </div>

                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a2.jpg">
                            </a>
                            <div class="media-body">
                                <a href="#">
                                    Andrew Williams
                                </a>
                                Making this the first true generator on the Internet. It uses a dictionary of.
                                <br />
                                <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 11 Like this!</a> -
                                <small class="text-muted">10.07.2014</small>
                            </div>
                        </div>

                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a3.jpg">
                            </a>
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Write comment..."></textarea>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="social-feed-box">

                    <div class="pull-right social-action dropdown">
                        <button data-toggle="dropdown" class="dropdown-toggle btn-white">
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu m-t-xs">
                            <li><a href="#">Config</a></li>
                        </ul>
                    </div>
                    <div class="social-avatar">
                        <a href="" class="pull-left">
                            <img alt="image" src="img/a6.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                Andrew Williams
                            </a>
                            <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                        </div>
                    </div>
                    <div class="social-body">
                        <p>
                            Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                            default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Packages and web page editors now use Lorem Ipsum as their
                            default model text.
                        </p>
                        <p>
                            Lorem Ipsum as their
                            default model text, and a search for 'lorem ipsum' will uncover many web sites still
                            in their infancy. Packages and web page editors now use Lorem Ipsum as their
                            default model text.
                        </p>
                        <img src="img/gallery/3.jpg" class="img-responsive">
                        <div class="btn-group">
                            <button class="btn btn-white btn-xs"><i class="fa fa-thumbs-up"></i> Like this!</button>
                            <button class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comment</button>
                            <button class="btn btn-white btn-xs"><i class="fa fa-share"></i> Share</button>
                        </div>
                    </div>
                    <div class="social-footer">
                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a1.jpg">
                            </a>
                            <div class="media-body">
                                <a href="#">
                                    Andrew Williams
                                </a>
                                Internet tend to repeat predefined chunks as necessary, making this the first true generator
                                on the Internet. It uses a dictionary of over 200 Latin words.
                                <br />
                                <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 26 Like this!</a> -
                                <small class="text-muted">12.06.2014</small>
                            </div>
                        </div>

                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a2.jpg">
                            </a>
                            <div class="media-body">
                                <a href="#">
                                    Andrew Williams
                                </a>
                                Making this the first true generator on the Internet. It uses a dictionary of.
                                <br />
                                <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 11 Like this!</a> -
                                <small class="text-muted">10.07.2014</small>
                            </div>
                        </div>

                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a8.jpg">
                            </a>
                            <div class="media-body">
                                <a href="#">
                                    Andrew Williams
                                </a>
                                Making this the first true generator on the Internet. It uses a dictionary of.
                                <br />
                                <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 11 Like this!</a> -
                                <small class="text-muted">10.07.2014</small>
                            </div>
                        </div>

                        <div class="social-comment">
                            <a href="" class="pull-left">
                                <img alt="image" src="img/a3.jpg">
                            </a>
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Write comment..."></textarea>
                            </div>
                        </div>

                    </div>

                </div>




            </div>
            <div class="col-lg-4 m-b-lg">
                <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                    <div class="vertical-timeline-block">
                        @if (!$fidel->cultives->isEmpty())
                            <div
                                class="vertical-timeline-icon @if ($fidel->cultives[0]->pivot->etat == 1)
                            {{ 'bg-success' }}
                            @else
                                {{ 'bg-warning' }}
                                @endif">
                               <i class="fa fa-university"></i>
                            </div>
                        @else
                            <div class="vertical-timeline-icon bg-danger">
                               <i class="fa fa-university"></i>
                            </div>
                        @endif

                        <div class="vertical-timeline-content">
                            <h2>Cultivé la vie de christ</h2>
                            <p>Une formation qui presente la vision,la mission de l'église.
                            </p>
                            {{-- <a href="#" class="btn btn-sm btn-primary"> More info</a> --}}
                            @if (!$fidel->cultives->isEmpty())
                                @if ($fidel->cultives[0]->pivot->etat == 1)
                                    <span class="vertical-date">
                                        Date fin : <br>
                                        <small>{{ \Carbon\Carbon::parse($fidel->cultives[0]->pivot->updated_at)->isoFormat('LL') }}</small>
                                    </span>
                                @else
                                    <span class="vertical-date">
                                        Date debut : <br>
                                        <small>{{ \Carbon\Carbon::parse($fidel->cultives[0]->pivot->created_at)->isoFormat('LL') }}</small>
                                    </span>
                        </div>
                        @endif
                        @endif
                    </div>
                    <div class="vertical-timeline-block">
                        @if (!$fidel->metamorpho->isEmpty())
                            <div
                                class="vertical-timeline-icon @if ($fidel->metamorpho[0]->pivot->etat == 1)
                            {{ 'bg-success' }}
                    @else
                            {{ 'bg-warning' }}
                        @endif">
                               <i class="fa fa-stethoscope"></i>
                            </div>
                        @else
                            <div class="vertical-timeline-icon bg-danger">
                               <i class="fa fa-stethoscope"></i>
                            </div>
                        @endif
                        <div class="vertical-timeline-content">
                            <h2>Metamorpho</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                been the industry's standard dummy text ever since.</p>
                            {{-- <a href="#" class="btn btn-sm btn-success"> Download document </a> --}}
                            @if (!$fidel->metamorpho->isEmpty())
                                @if ($fidel->metamorpho[0]->pivot->etat == 1)
                                    <span class="vertical-date">
                                        Date fin : <br>
                                        <small>{{ \Carbon\Carbon::parse($fidel->metamorpho[0]->pivot->updated_at)->isoFormat('LL') }}</small>
                                    </span>
                                @else
                                    <span class="vertical-date">
                                        Date debut : <br>
                                        <small>{{ \Carbon\Carbon::parse($fidel->metamorpho[0]->pivot->created_at)->isoFormat('LL') }}</small>
                                    </span>
                        
                        @endif
                        @endif
                    </div>
                </div>

                <div class="vertical-timeline-block">
                    @if (!$fidel->ecap->isEmpty())
                    <div
                        class="vertical-timeline-icon @if ($fidel->ecap[0]->pivot->etat == 1)
            {{ 'bg-success' }}
            @else
                {{ 'bg-warning' }}
                @endif">
                     <i class="fa fa-mortar-board"></i>
                    </div>
                @else
                    <div class="vertical-timeline-icon bg-danger">
                     <i class="fa fa-mortar-board"></i>
                    </div>
                @endif
                    <div class="vertical-timeline-content">
                        <h2>Ecole d'apolos</h2>
                        <p>Go to shop and find some products. Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's. </p>
                            @if (!$fidel->ecap->isEmpty())
                            @if ($fidel->ecap[0]->pivot->etat == 1)
                                <span class="vertical-date">
                                    Date fin : <br>
                                    <small>{{ \Carbon\Carbon::parse($fidel->ecap[0]->pivot->updated_at)->isoFormat('LL') }}</small>
                                </span>
                            @else
                                <span class="vertical-date">
                                    Date debut : <br>
                                    <small>{{ \Carbon\Carbon::parse($fidel->ecap[0]->pivot->created_at)->isoFormat('LL') }}</small>
                                </span>
                    
                    @endif
                    @endif
                    </div>
                </div>

                <div class="vertical-timeline-block">
                    @if (!$fidel->gifted->isEmpty())
                    <div
                        class="vertical-timeline-icon @if ($fidel->gifted[0]->pivot->etat == 1)
            {{ 'bg-success' }}
            @else
                {{ 'bg-warning' }}
                @endif">
                        <i class="fa fa-gift"></i>
                    </div>
                @else
                    <div class="vertical-timeline-icon bg-danger">
                        <i class="fa fa-gift"></i>
                    </div>
                @endif
                    <div class="vertical-timeline-content">
                        <h2>Gifted</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident
                            rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus
                            minus veritatis qui ut.</p>
                            @if (!$fidel->gifted->isEmpty())
                            @if ($fidel->gifted[0]->pivot->etat == 1)
                                <span class="vertical-date">
                                    Date fin : <br>
                                    <small>{{ \Carbon\Carbon::parse($fidel->gifted[0]->pivot->updated_at)->isoFormat('LL') }}</small>
                                </span>
                            @else
                                <span class="vertical-date">
                                    Date debut : <br>
                                    <small>{{ \Carbon\Carbon::parse($fidel->gifted[0]->pivot->created_at)->isoFormat('LL') }}</small>
                                </span>
                    
                    @endif
                    @endif
                    </div>
                </div>
            </div>

        </div>

    </div>
    </div>
@endsection
@section('autres_script')
    <script src="{{ asset('js/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('js/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/jasny/jasny-bootstrap.min.js') }}"></script>


    <script src="{{ asset('js/parsley/js/parsley.js') }}"></script>
    <script src="{{ asset('js/parsley/i18n/fr.js') }}"></script>

    <script src="{{ asset('js/toastr/toastr.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#formFidel").on("submit", function(e) {
                e.preventDefault();
                add("#formFidel", '#tab-fidel', 'createSessionCVC')
            });
        });

        function load(id) {
            $(id).children('.ibox-content').toggleClass('sk-loading');
        }

        function add(form, idLoad, url) {
            var f = form;
            var loade = idLoad;
            var u = url;
            load(loade);
            $.ajax({
                url: u,
                method: "POST",
                data: $(f).serialize(),
                success: function(data) {
                    load(loade);
                    if (!data.reponse) {
                        swal({
                            title: data.msg,
                            icon: 'error'
                        })
                    } else {
                        swal({
                            title: data.msg,
                            icon: 'success'
                        })

                        $(f)[0].reset();
                    }

                },
            });

        }
    </script>
@endsection
