<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Philacademia</title>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="SovWare" />
        <meta name="description" content="Study any topic, anytime. explore thousands of courses for the lowest price ever!" />

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logophila.png') }}">

        <link rel="stylesheet" href="assets/css/jquery.webui-popover.min.css" />
        <link rel="stylesheet" href="assets/css/slick.css" />
        <link rel="stylesheet" href="assets/css/slick-theme.css" />

        <!-- font awesome 5 -->
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css" />

        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/responsive.css" />
        <link rel="stylesheet" href="assets/css/custom.css" />
        <script src="assets/js/jquery-3.3.1.min.js"></script>

        @yield("autres_style")
    </head>
    <body class="gray-bg">
        <div class="bg-white menu-area">
            <div class="container-xl">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <ul class="mobile-header-buttons">
                        <li>
                            <a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a>
                        </li>
                        <li>
                            <a class="mobile-search-trigger" href="#mobile-search">Search<span></span></a>
                        </li>
                    </ul>

                    <a href="{{ route('accueil') }}" class="navbar-brand">
                        <img src="{{ asset('img/logophila.png') }}" alt="logo" height="35" />
                    </a>

                    <div class="main-nav-wrap">
                        <div class="mobile-overlay"></div>

                        <ul class="mobile-main-nav">
                            <li class="mobile-menu-helper-top"></li>
                            <li class="has-children text-nowrap fw-bold">
                                <a href="">
                                    <i class="fas fa-th d-inline text-20px"></i>
                                    <span class="fw-500">Menu</span>
                                    <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                </a>

                                <ul class="pb-0 category corner-triangle top-left is-hidden">
                                    <li class="go-back">
                                        <a href=""><i class="fas fa-angle-left"></i>Menu</a>
                                    </li>

                                    <li class="has-children">
                                        <a href="course-grid.html" class="py-2">
                                            <span class="icon"><i class="fas fa-pencil-alt"></i></span>
                                            <span>Liste des cours</span>
                                            <span class="has-sub-category"><i class="fas fa-angle-right"></i></span>
                                        </a>
                                        <ul class="sub-category is-hidden">
                                            <li class="go-back-menu">
                                                <a href=""><i class="fas fa-angle-left"></i>Menu</a>
                                            </li>
                                            <li class="go-back">
                                                <a href="">
                                                    <i class="fas fa-angle-left"></i>
                                                    <span class="icon"><i class="fas fa-pencil-alt"></i></span>
                                                  Liste des cours
                                                </a>
                                            </li>
                                            <li><a href="{{ route('detail') }}">Nouvelle naissance</a></li>
                                            <li><a href="{{ route('detail') }}">Eglise</a></li>
                                            <li><a href="{{ route('detail') }}">Meditation</a></li>
                                            <li><a href="{{ route('detail') }}">Ecoute de Dieu</a></li>
                                            <li><a href="{{ route('detail') }}">Don spirituel</a></li>
                                        </ul>
                                    </li>
                                    <li class="p-0 mb-0 all-category-devided">
                                        <a href="{{ route('loginFidel') }}" class="py-2"><span>Connexion</span></a>                                       
                                    </li>
                                    <li class="p-0 mb-0 all-category-devided">
                                        <a href="{{ route('registerFidel') }}" class="py-2"><span>Inscription</span></a>                                       
                                    </li>
                                </ul>
                            </li>

                            <li class="mobile-menu-helper-bottom"></li>
                        </ul>
                    </div>

                    <form class="inline-form" action="#" method="get" style="width: 100%;">
                        <div class="input-group search-box mobile-search">
                            <input type="text" name="query" class="form-control" placeholder="Search for courses" />
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>

                    <div class="instructor-box menu-icon-box">
                        <div class="icon">
                            <a
                                href="{{ route('mesCours') }}"
                                style="border: 1px solid transparent; margin: 0px; padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;"
                            >
                              Mon parcours
                            </a>
                        </div>
                    </div>

                    <div class="instructor-box menu-icon-box">
                        <div class="icon">
                            <a
                                href="{{ route('mesCours') }}"
                                style="border: 1px solid transparent; margin: 0px; padding: 0px 10px; font-size: 14px; width: max-content; border-radius: 5px; height: 40px; line-height: 40px;"
                            >
                                Mes cours
                            </a>
                        </div>
                    </div>

                    <div class="user-box menu-icon-box">
                        <div class="icon">
                            <a href="javascript::">
                                <img src="assets/images/uploads/user_image/placeholder.png" alt="placeholder" class="img-fluid" />
                            </a>
                        </div>
                        <div class="dropdown user-dropdown corner-triangle top-right">
                            <ul class="user-dropdown-menu">
                                <li class="dropdown-user-info">
                                    <a href="">
                                        <div class="clearfix">
                                            <div class="user-image float-start">
                                                <img src="assets/images/uploads/user_image/placeholder.png" alt="user_image" />
                                            </div>
                                            <div class="user-details">
                                                <div class="user-name">
                                                    <span class="hi">Hi,</span>
                                                    Ben Hanson
                                                </div>
                                                <div class="user-email">
                                                    <span class="email">student@example.com</span>
                                                    <span class="welcome">Welcome back</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="user-dropdown-menu-item">
                                    <a href="{{ route('mesCours') }}"><i class="far fa-gem"></i>Mes cours</a>
                                </li>
                                <li class="user-dropdown-menu-item">
                                    <a href="{{ route('mesCours') }}"><i class="far fa-heart"></i>Mes traveaux</a>
                                </li>
                                <li class="user-dropdown-menu-item">
                                    <a href="{{ route('mesCours') }}"><i class="fas fa-user"></i>Profil</a>
                                </li>

                                <li class="dropdown-user-logout user-dropdown-menu-item"><a href="{{ route('mesCours') }}">Déconnexion</a></li>
                            </ul>
                        </div>
                    </div>

                    <span class="signin-box-move-desktop-helper"></span>
                    <div class="sign-in-box btn-group d-none">
                        <button type="button" class="btn btn-sign-in" data-toggle="modal" data-target="#signInModal">Log In</button>

                        <button type="button" class="btn btn-sign-up" data-toggle="modal" data-target="#signUpModal">Sign Up</button>
                    </div>
                    <!--  sign-in-box end -->
                </nav>
            </div>
        </div>
@yield('content')

        <footer class="mt-5 footer-area d-print-none bg-gray">
           
            <div class="border-top">
                <div class="container-xl">
                    <div class="py-1 mt-3 row">
                        <div class="col-6 col-sm-6 col-md-3 text-muted text-13px">
                            &copy; 2022 Phila CE, All rights reserved
                        </div>

                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="col-6 col-sm-6 col-md-3 d-none d-md-block"></div>
                        <div class="text-center col-6 col-sm-6 col-md-3 text-md-start">
                            <select class="language_selector" onchange="switch_language(this.value)">
                                <option value="arabic">Arabic</option>
                                <option value="english" selected>English</option>
                                <option value="french">French</option>
                                <option value="german">German</option>
                                <option value="hindi">Hindi</option>
                                <option value="indonesian">Indonesian</option>
                                <option value="japanese">Japanese</option>
                                <option value="ru">Ru</option>
                                <option value="spanish">Spanish</option>
                                <option value="turkish">Turkish</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="assets/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/jquery.webui-popover.min.js"></script>

        <script src="assets/js/main.js"></script>
        <script src="assets/js/jquery.form.min.js"></script>


        <script>
            function isTouchDevice() {
                return "ontouchstart" in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
            }

            //Event call after loading page
            document.addEventListener(
                "DOMContentLoaded",
                function () {
                    setTimeout(function () {
                        $(".animated-loader").hide();
                        $(".shown-after-loading").show();
                    });
                },
                false
            );
        </script>
         @yield("autres_script")
    </body>
</html>
