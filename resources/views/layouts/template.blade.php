
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} | {{ isset($titre) ? $titre : '' }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" href="{{ asset('img/logophila.png') }}" type="image/rdp-icon">

    <!-- Styles -->

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('js/sweetalert/sweetalert.css') }}" rel="stylesheet">

    @yield("autres_style")
</head>

<body class="">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span>
                                <img alt="image" class="img-circle" src="{{ asset('img/default.png') }}"
                                    width="100" />
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong
                                            class="font-bold">{{ Auth::user()->prenom . '-' . Auth::user()->name }}</strong>
                                    </span> <span class="block text-xs text-muted">{{ Auth::user()->fonction }} <b
                                            class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="">Profile</a></li>
                                <li><a href="">Envoyer rapport</a></li>
                                <li class="divider"></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Déconnexion
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            PHILA
                        </div>
                    </li>
                    <li class="{{ $titre == 'Accueil' ? 'active' : '' }}">
                        <a href="{{ route('dashboard') }}"><i class="fa fa-home"></i>
                            <span class="nav-label">Accueil</span></a>
                    </li>
                    <li class="{{ $titre == 'Enregistrement fidèle' ? 'active' : '' }}">
                        <a href="{{ route('addFidele') }}"><i class="fa fa-home"></i>
                            <span class="nav-label">Enregistrez un fidèle</span></a>
                    </li>
                    <li class="{{ $titre == 'Mon smallgroup' ? 'active' : '' }}">
                        <a href="{{ route('MySmallgroup') }}"><i class="fa fa-group"></i>
                            <span class="nav-label">Mon Smallgroup</span></a>
                    </li>
                    <li class="{{ $titre == 'Page rapport' ? 'active' : '' }}">
                        <a href="{{ route('viewPageRapport') }}"><i class="fa fa-pencil-square-o"></i>
                            <span class="nav-label">Envoyer Rapport</span></a>
                    </li>
                    <li class="{{ $titre == 'Fidèles' ? 'active' : '' }}">
                        <a href="{{ route('fideles') }}"><i class="fa fa-group"></i>
                            <span class="nav-label">Gestion des Fidèles</span></a>
                    </li> 
                    <li class="{{ $titre2 === 'CVC' || $titre === 'Gestion-Cultivé' ? 'active' : '' }}">
                        <a href=""><i class="fa fa-university"></i> <span class="nav-label">Cultiver la vie de christ (CVC)</span> <span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="{{ $titre == 'Cultivé' ? 'active' : '' }}"><a href="{{ route('cultive') }}">Crée
                                    une session</a></li>
                            <li class="{{ $titre == 'Gestion-CVC' ? 'active' : '' }}"><a
                                    href="{{ route('gestioncvc') }}">Gestion CVC</a></li>
                            <li class="{{ $titre == 'Gestion-Cultivé' ? 'active' : '' }}"><a
                                    href="{{ route('g_cultive') }}">enregistrer pour CVC</a></li>
                            <li class="{{ $titre == 'Début-metamorpho' ? 'active' : '' }}"><a
                                    href="{{ route('debutMeta') }}">Envoyer à metamorpho</a></li>
                        </ul> 
                    </li>
                    <li class="{{ $titre2== 'metamorpho' ? 'active' : '' }}">
                        <a href=""><i class="fa fa-stethoscope"></i> <span class="nav-label">METAMORPHOO</span> <span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="{{ $titre == 'Session' ? 'active' : '' }}"><a
                                    href="{{ route('metamorpho') }}">Session</a></li>
                            <li class="{{ $titre == 'Gestion mentor' ? 'active' : '' }}"><a
                                    href="{{ route('gMentor') }}">Gestion des mentors</a></li>
                            <li class="{{ $titre == 'Smallgroup' ? 'active' : '' }}"><a
                                    href="{{ route('addSmallgroup') }}">Smallgroups</a></li>
                            <li class="{{ $titre == 'Affectation dans smallgroup' ? 'active' : '' }}"><a
                                    href="{{ route('gestionSmallgroup') }}">Repartir dans smallgroups</a>
                            </li>
                            <li class="{{ $titre == 'Gestion des rapports' ? 'active' : '' }}"><a
                                    href="{{ route('viewPageGestionRapport') }}">Gestion des rapports</a></li>
                            <li class="{{ $titre == 'Passer à ECAP' ? 'active' : '' }}"><a
                                    href="{{ route('debutEcap') }}">Envoyer à l'ECAP</a></li>
                        </ul>
                    </li>
                    <li class="{{ $titre == 'Ecap' ? 'active' : '' }}">
                        <a href="{{ route('ecap') }}"><i class="fa fa-mortar-board"></i>
                            <span class="nav-label">Ecole d'Apollos (ECAP)</span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="{{ $titre == 'Session' ? 'active' : '' }}"><a
                                    href="{{ route('metamorpho') }}">Configuré </a></li>
                            <li class="{{ $titre == 'Gestion mentor' ? 'active' : '' }}"><a
                                    href="{{ route('gMentor') }}">Gestion des étudiants </a></li>
                            <li class="{{ $titre == 'Smallgroup' ? 'active' : '' }}"><a
                                    href="{{ route('addSmallgroup') }}">Gestion des cours</a></li>
                            <li class="{{ $titre == 'Affectation dans smallgroup' ? 'active' : '' }}"><a
                                    href="{{ route('gestionSmallgroup') }}">Gestion des enseignants</a>
                            </li>
                            <li class="{{ $titre == 'Gestion des rapports' ? 'active' : '' }}"><a
                                    href="{{ route('viewPageGestionRapport') }}">Gestion des travaux</a></li>
                            <li class="{{ $titre == 'Gestion des rapports' ? 'active' : '' }}"><a
                                    href="{{ route('viewPageGestionRapport') }}">Gestion des examens et test</a></li>
                            <li class="{{ $titre == 'Passer à ECAP' ? 'active' : '' }}"><a
                                    href="{{ route('debutEcap') }}">Envoyer à Gifted</a></li>
                        </ul>
                    </li>

                    <li class="{{ $titre == 'Gifted' ? 'active' : '' }}">
                        <a href="{{ route('gifted') }}"><i class="fa fa-gift"></i>
                            <span class="nav-label">Gifted</span></a>
                    </li>
                    <li class="{{ $titre2 == 'Users' ? 'active' : '' }}">
                        <a href=""><i class="fa fa-users"></i><span class="nav-label">Users</span> <span
                            class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li class="{{ $titre == 'Ajouter un user' ? 'active' : '' }}">
                                    <a href="{{ route('g_user') }}">
                                        <span class="nav-label">Creer user</span></a>
                                </li>
                                <li class="{{ $titre == 'Gestion des users' ? 'active' : '' }}">
                                    <a href="{{ route('g_user') }}">
                                        <span class="nav-label">Gestion user</span></a>
                                </li>

                            </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i
                                class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            {{-- <div class="form-group">
                                        <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                    </div> --}}
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Bienvenue dans l'espace Admin Cursus PHILA
                                ACADEMIA.</span>
                        </li>

                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Déconnexion
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>

                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>{{ $titre }}</h2>
                </div>
            </div>

            @yield('content')

            <div class="footer">
                <div class="pull-right">

                </div>
                <div>
                    <a href='silasdev.com'> <strong>Copyright</strong> SDev &copy; 2021-2022 </a>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/pace/pace.min.js') }}"></script>

    <script src="{{ asset('js/sweetalert/sweetalert.min.js') }}"></script>

    @yield("autres_script")
</body>

</html>
