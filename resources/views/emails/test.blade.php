@extends('emails.template')


@section('content')

<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <img class="img-responsive" src="{{asset('img/logophila.png')}}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3>Bienvenu(e) {{$data->prenom.' '.$data->nom}} </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                   Vous êtes inscrit pour suivre le cursus de PHILA ACADEMIA.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        Vous serez notifiez pour vous annoncez du debut des enseignement.
                                         </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        Que Dieu vous bénisse.
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <td class="content-block aligncenter">
                                        <a href="#" class="btn-primary">Confirm email address</a>
                                    </td>
                                </tr> --}}
                              </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        {{-- <tr>
                            <td class="aligncenter content-block">Follow <a href="#">@Company</a> on Twitter.</td>
                        </tr> --}}
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>

@endsection