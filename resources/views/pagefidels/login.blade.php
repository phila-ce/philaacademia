
@extends('layouts.templateEclasse',['titre'=>"Enregistrement fidèle",'titre2'=>"ajout"])


@section('content')
   
<section class="category-course-list-area">
    <div class="container">
        <div class="mt-3 mb-5 row">
            <div class="text-center col-md-12">
                <h1 class="fw-700">Connexion</h1>
                <p class="text-14px">Fournissez vos identifiants de connexion valides</p>
            </div>
        </div>
        <div class="row">
            <div class="text-center col-lg-6 d-none d-lg-block">
                <img class="mt-4" width="60%" src="assets/images/system/sign_up.png" />
            </div>
            <div class="col-lg-6">
                <div class="sign-up-form">
                   

                    <div class="form-group">
                        <div class="row justify-content-center">
                            <div class="col-3 d-none d-sm-block">
                                <hr class="w-100" />
                            </div>
                            <div class="col-auto pt-1 text-center text-13px fw-500 text-muted">
                                Continue with email
                            </div>
                            <div class="col-3 d-none d-sm-block">
                                <hr class="w-100" />
                            </div>
                        </div>
                    </div>
                    <form action="" method="post" id="sign_up">
                        <div class="form-group">
                            <label for="login-email">Email</label>
                            <div class="input-group">
                                <span class="bg-white input-group-text" for="email"><i class="fas fa-user"></i></span>
                                <input type="email" name="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="Email" id="login-email" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="login-password">Password</label>
                            <div class="input-group">
                                <span class="bg-white input-group-text" for="password"><i class="fas fa-user"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="Password" id="login-password" required />
                            </div>
                            <a class="text-muted text-12px fw-500 float-end" href="#">Forgot password?</a>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="mt-4 btn red radius-10 w-100">Connexion</button>
                        </div>

                        <div class="mt-4 mb-0 text-center form-group">
                            Do not have an account?
                            <a class="text-15px fw-700" href="{{ route('registerFidel') }}">Sign up</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection