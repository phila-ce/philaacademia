@extends('layouts.templateEclasse',['titre'=>"Enregistrement fidèle",'titre2'=>"ajout"])


@section('content')
<section class="course-header-area">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="course-header-wrap">
                    <h1 class="title">Nouvelle naissance</h1>
                    <p class="subtitle">Tout commence par la nouvelle naissance</p>
                    <div class="rating-row">
                        <span class="course-badge best-seller">Beginner</span>
                        <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                        <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                        <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                        <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                        <i class="fas fa-star"></i>
                        <span class="d-inline-block average-rating">4</span><span>(1 Ratings)</span>
                        <span class="enrolled-num"> 1 Students enrolled </span>
                        <span class="comment"><i class="fas fa-comment"></i>English</span>
                    </div>
                    <div class="created-row">
                        <span class="created-by"> Created by <a class="text-14px fw-600 text-decoration-none" href="instructor.html">Dave Franco</a> </span>
                        <br />
                        <span class="mt-2 last-updated-date d-inline-block">Last updated Thu, 27-May-2021</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
</section>

<section class="course-content-area">
    <div class="container">
        <div class="row">
            <div class="order-last mt-4 bg-white col-lg-8 order-lg-first radius-10 p-30-40">
                <div class="description-box view-more-parent">
                    <div class="view-more" onclick="viewMore(this,'hide')">+ Lire plus</div>
                    <div class="description-title">Aperçu du cours</div>
                    <div class="description-content-wrap">
                        <div class="description-content">
                            <div data-purpose="safely-set-inner-html:description:description" xss="removed">
                                <p xss="removed">
                                    Learn how to use Adobe Illustrator to draw fashion flats. Develop your skills to enable you to produce creative, accurate product designs quickly and to standards required for retail and
                                    manufacturing.
                                </p>
                                <p xss="removed"><strong xss="removed">In this beginners level course, you will learn all the core tools and features that make up the basic fashion drawing toolbox.</strong></p>
                                <ul xss="removed">
                                    <li xss="removed">For beginners that have not used Adobe Illustrator before</li>
                                    <li xss="removed">Overview of Illustrator terminology</li>
                                    <li xss="removed">Overview of design and product development processes used in industry and how Illustrator fits into the design workflow</li>
                                    <li xss="removed">Familiarise yourself with the Illustrator interface</li>
                                    <li xss="removed">Learn the core tools and features needed to produce basic fashion drawings</li>
                                    <li xss="removed">Garment drawing workshops</li>
                                </ul>
                                <p xss="removed"></p>
                                <p xss="removed"><strong xss="removed">Learn essential skills required by the fashion industry</strong></p>
                                <p xss="removed">
                                    Having spent over a decade implementing and training software technologies into fashion brands and retailers across the globe, I spent a lot of time working with the designers. Over the years
                                    there seemed to be a common theme in the design room - lots of designers were using Illustrator well but many either couldn't use it at all or couldn't use it very well. They also didn't adhere to
                                    any drawing standards or best practises so had problems such as not being able to easily edit each other's drawings, create colourways quickly or create reusable components properly, making them
                                    inefficient.
                                </p>
                                <p xss="removed">
                                    Illustrator 4 Fashion training has been specifically tailored for fashion, with the needs of the industry in mind. The training can be taken by company design employees, freelance designers,
                                    fashion design students and anyone else looking to learn or improve their fashion drawing with Adobe Illustrator.
                                </p>
                                <p xss="removed"><strong xss="removed">Course Overview</strong></p>
                                <p xss="removed">
                                    Most Adobe Illustrator manuals and online training courses are industry generic with many modules not relevant to fashion drawing. It can be hard to know which features to learn and how to develop
                                    techniques to draw professional fashion designs. Illustrator 4 Fashion only teaches you the features and techniques relevant to drawing fashion flats, illustrations and garment components.
                                </p>
                                <p xss="removed">
                                    The Illustrator 4 Fashion course is split into 3 levels; beginner, intermediate and advanced. In this level 1 beginners course, aimed at users with no previous Illustrator knowledge, we start off
                                    by introducing you to the Illustrator terminology and interface. You'll also gain an understanding of the fashion product development process used in industry and how Adobe Illustrator is used as
                                    part of this. Following this we will then go through in simple individual tutorials all the functional tools you need to know how to use to get you started with fashion drawing. The final part of
                                    the course features a series of garment drawing workshops where we will put together all the skills you have acquired into practise.
                                </p>
                                <p xss="removed">Every lecture includes supporting Illustrator work files so that students can follow the lecture exactly.</p>
                            </div>
                            <div class="styles--audience--2pZ0S" data-purpose="target-audience" xss="removed">
                                <h2 class="udlite-heading-xl styles--audience__title--1Sob_" xss="removed">Who this course is for:</h2>
                                <ul class="styles--audience__list--3NCqY" xss="removed">
                                    <li xss="removed">Suitable for fashion design students</li>
                                    <li xss="removed">Suitable for freelance fashion designers</li>
                                    <li xss="removed">Suitable for company fashion design employees</li>
                                    <li xss="removed">Suitable for anyone looking to learn how to draw fashion using Adobe Illustrator</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="py-3">Que vais-je apprendre ?</h4>
                <div class="what-you-get-box">
                    <ul class="what-you-get__items">
                        <li>You will have a good foundation in drawing fashion flats</li>
                        <li>You will be ready to progress to the intermediate and advanced training with the details and links to these additional online courses provided in the course</li>
                        <li>You will be well on your way to becoming a fashion designer with expert skills in Adobe Illustrator.</li>
                        <li>You will stand out from the thousands of fashion design graduates and make yourself more attractive to potential employers.</li>
                    </ul>
                </div>

                <div class="requirements-box">
                    <div class="requirements-title">Requirements</div>
                    <div class="requirements-content">
                        <ul class="requirements__list">
                            <li>
                                You should have Adobe Illustrator installed. The training is designed around the latest Adobe Creative Cloud version of Illustrator, however you can still follow the training with older versions of
                                Illustrator as many of the techniques will be the same. You should be aware though that there may be some functions featured that will not be available in older versions.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="course-curriculum-box">
                    <div class="clearfix mt-5 mb-3 course-curriculum-title">
                        <div class="title float-start">Table de matière</div>
                        <div class="mt-2 float-end">
                            <span class="total-lectures"> 7 Lessons </span>
                            <span class="total-time"> 00:43:50 Hours </span>
                        </div>
                    </div>
                    <div class="course-curriculum-accordion">
                        <div class="lecture-group-wrapper">
                            <div class="clearfix lecture-group-title" style="border-radius: 10px 10px 0px 0px;" data-bs-toggle="collapse" data-bs-target="#collapse91" aria-expanded="true">
                                <div class="title float-start">
                                    Introduction
                                </div>
                                <div class="float-end">
                                    <span class="total-lectures"> 5 Lessons </span>
                                    <span class="total-time"> 00:29:14 Hours </span>
                                </div>
                            </div>

                            <div id="collapse91" class="lecture-list collapse show">
                                <ul>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title" onclick="go_course_playing_page('12', '157')">Course Overview</span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-time ps-2"> 00:16:30 </span>
                                        </div>
                                    </li>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title" onclick="go_course_playing_page('12', '287')">Illustrator Fashion Design Tutorial: How to Draw a Fashion</span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-time ps-2"> 00:10:08 </span>
                                        </div>
                                    </li>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title" onclick="go_course_playing_page('12', '289')">Fundamental HTML Block is known as </span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-time ps-2"> 00:00:00 </span>
                                        </div>
                                    </li>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title text-primary" onclick="go_course_playing_page('12', '286')">Design and Product Development Workflow</span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-preview" onclick="lesson_preview('#', 'Lesson: Design and Product Development Workflow')">
                                                <i class="fas fa-eye"></i>
                                                Preview
                                            </span>

                                            <span class="lecture-time ps-2"> 00:02:36 </span>
                                        </div>
                                    </li>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title" onclick="go_course_playing_page('12', '288')">Color Fashion Sketches in Illustrator</span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-time ps-2"> <span class="opacity-0">.</span> </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="lecture-group-wrapper">
                            <div class="clearfix lecture-group-title" style="border-radius: 0px 0px 10px 10px;" data-bs-toggle="collapse" data-bs-target="#collapse96" aria-expanded="">
                                <div class="title float-start">
                                    Fashion Drawing Workshops
                                </div>
                                <div class="float-end">
                                    <span class="total-lectures"> 2 Lessons </span>
                                    <span class="total-time"> 00:14:36 Hours </span>
                                </div>
                            </div>

                            <div id="collapse96" class="lecture-list collapse">
                                <ul>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title" onclick="go_course_playing_page('12', '290')">Web technologies pop quiz</span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-time ps-2"> 00:00:00 </span>
                                        </div>
                                    </li>
                                    <li class="lecture has-preview text-14px">
                                        <span class="lecture-title" onclick="go_course_playing_page('12', '160')">Illustrator Shading Tutorial: Make Your Fashion Drawings 3D with Folds and Shadows</span>

                                        <div class="lecture-info float-lg-end">
                                            <span class="lecture-time ps-2"> 00:14:36 </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="compare-box view-more-parent">
                    <div class="view-more" onclick="viewMore(this)">+ Lire plus</div>
                    <div class="compare-title">Other related courses</div>
                    <div class="compare-courses-wrap"></div>
                </div>

                <div class="about-instructor-box">
                    <div class="about-instructor-title">
                        About instructor
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4 top-instructor-img w-sm-100">
                            <a href="instructor.html">
                                <img src="assets/images/uploads/user_image/0269091217f95c25ac4f77c1bd69879a.jpg" width="100%" />
                            </a>
                        </div>
                        <div class="text-center col-md-8 top-instructor-details text-md-start">
                            <h4 class="mb-1 fw-600 v"><a class="text-decoration-none" href="instructor.html">Dave Franco</a></h4>
                            <p class="fw-500 text-14px w-100"></p>
                            <div class="rating">
                                <div class="d-inline-block">
                                    <span class="text-dark fw-800 text-muted ms-1 text-13px">4 Reviews</span>
                                    |
                                    <span class="mx-1 text-dark fw-800 text-13px text-muted"> 1 Students </span>
                                    |
                                    <span class="text-dark fw-800 text-14px text-muted"> 6 Courses </span>
                                </div>
                            </div>
                            <span class="py-2 my-1 badge badge-sub-warning text-12px"></span>

                            <div class="description">
                                Hi, I'm Dave! I have been identified as one of the Edustar Top Instructors and all my premium courses have recently earned them best-selling status for outstanding performance ...
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pb-3 mt-5 student-feedback-box">
                    <div class="student-feedback-title">
                        Student feedback
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="average-rating ms-auto me-auto float-md-start mb-sm-4">
                                <div class="num">
                                    4
                                </div>
                                <div class="rating">
                                    <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                    <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                    <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                    <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                    <i class="fas fa-star" style="color: #abb0bb;"></i>
                                </div>
                                <div class="title text-15px fw-700">1 Reviews</div>
                            </div>
                            <div class="individual-rating">
                                <ul>
                                    <li>
                                        <div>
                                            <span class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star filled"></i>
                                            </span>
                                        </div>
                                        <div class="mt-1 progress ms-2">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                        <span class="d-inline-block ps-2 text-15px fw-500">
                                            (0)
                                        </span>
                                    </li>
                                    <li>
                                        <div>
                                            <span class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                            </span>
                                        </div>
                                        <div class="mt-1 progress ms-2">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                        <span class="d-inline-block ps-2 text-15px fw-500">
                                            (0)
                                        </span>
                                    </li>
                                    <li>
                                        <div>
                                            <span class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                            </span>
                                        </div>
                                        <div class="mt-1 progress ms-2">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                        <span class="d-inline-block ps-2 text-15px fw-500">
                                            (0)
                                        </span>
                                    </li>
                                    <li>
                                        <div>
                                            <span class="rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                            </span>
                                        </div>
                                        <div class="mt-1 progress ms-2">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                        <span class="d-inline-block ps-2 text-15px fw-500">
                                            (1)
                                        </span>
                                    </li>
                                    <li>
                                        <div>
                                            <span class="rating">
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                                <i class="fas fa-star filled"></i>
                                            </span>
                                        </div>
                                        <div class="mt-1 progress ms-2">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                        <span class="d-inline-block ps-2 text-15px fw-500">
                                            (0)
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="mt-5 reviews">
                        <h3>Reviews</h3>
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-auto">
                                        <div class="clearfix reviewer-details">
                                            <div class="reviewer-img">
                                                <img src="assets/images/uploads/user_image/placeholder.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="review-time">
                                            <div class="reviewer-name fw-500">
                                                Ben Hanson
                                            </div>
                                            <div class="time text-11px text-muted">01/06/2021</div> 
                                        </div>
                                        <div class="review-details">
                                            <div class="rating">
                                                <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                                <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                                <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                                <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                                                <i class="fas fa-star" style="color: #abb0bb;"></i>
                                            </div>
                                            <div class="review-text text-13px">
                                                Future updates will make this course better.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="order-first col-lg-4 order-lg-last">
                <div class="course-sidebar natural">
                    <div class="preview-video-box">
                        <a data-bs-toggle="modal" data-bs-target="#CoursePreviewModal">
                            <img src="assets/images/uploads/thumbnails/course_thumbnails/course_thumbnail_default_12.jpg" alt="" class="w-100" />
                            <span class="preview-text">Preview this course</span>
                            <span class="play-btn"></span>
                        </a>
                    </div>
                    <div class="course-sidebar-text-box">
                        

                        <!-- WISHLIST BUTTON -->
                        <div class="buy-btns">
                            <button class="btn btn-add-wishlist" type="button" id="12" onclick="handleAddToWishlist(this)">
                             Commencer
                            </button>
                        </div>

                        <div class="buy-btns">
                            <button class="btn btn-buy-now" type="button" id="12" onclick="handleCartItems(this)">Impressions</button>
                        </div>

                        <div class="includes">
                            <div class="title"><b>A savoir :</b></div>
                            <ul>
                                <li><i class="far fa-file-video"></i> 00:43:50 Horaires Vidéos à la demande</li>
                                <li><i class="far fa-file"></i>7 Leçons</li>
                                <li><i class="fas fa-mobile-alt"></i>Accès sur mobile et tv</li>
                                <li><i class="far fa-compass"></i>Accès libre 24h/7j</li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="CoursePreviewModal" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content course-preview-modal">
            <div class="modal-header">
                <h5 class="modal-title"><span>Course preview:</span>Learn to draw fashion with Adobe Illustrator CC - Beginners</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <!-- <button type="button" class="close" data-bs-dismiss="modal" onclick="pausePreview()">
    <span aria-hidden="true">&times;</span>
  </button> -->
            </div>
            <div class="modal-body">
                <div class="course-preview-video-wrap">
                    <div class="embed-responsive embed-responsive-16by9">
                        <!------------- PLYR.IO ------------>
                        <link rel="stylesheet" href="assets/global/plyr/plyr.css" />

                        <div class="plyr__video-embed" id="player">
                            <iframe
                                height="500"
                                src="https://youtu.be/Ib8UBwu3yGA?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
                                allowfullscreen
                                allowtransparency
                                allow="autoplay"
                            ></iframe>
                        </div>

                        <script src="assets/global/plyr/plyr.js"></script>
                        <script>
                            const player = new Plyr("#player");
                        </script>
                        <!------------- PLYR.IO ------------>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->

<style media="screen">
    .embed-responsive-16by9::before {
        padding-top: 0px;
    }
</style>


@endsection

@section('autres_script')
<script type="text/javascript">
    function isTouchDevice() {
        return "ontouchstart" in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
    }

    function viewMore(element, visibility) {
        if (visibility == "hide") {
            $(element).parent(".view-more-parent").addClass("expanded");
            $(element).remove();
        } else if ($(element).hasClass("view-more")) {
            $(element).parent(".view-more-parent").addClass("expanded has-hide");
            $(element).removeClass("view-more").addClass("view-less").html("- View less");
        } else if ($(element).hasClass("view-less")) {
            $(element).parent(".view-more-parent").removeClass("expanded has-hide");
            $(element).removeClass("view-less").addClass("view-more").html("+ View more");
        }
    }

    //Event call after loading page
    document.addEventListener(
        "DOMContentLoaded",
        function () {
            setTimeout(function () {
                $(".animated-loader").hide();
                $(".shown-after-loading").show();
            });
        },
        false
    );
</script>

@endsection