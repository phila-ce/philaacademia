

@extends('layouts.templateEclasse',['titre'=>"Enregistrement fidèle",'titre2'=>"ajout"])


@section('content')
   
<section class="category-course-list-area">
    <div class="container">
        <div class="mt-3 mb-5 row">
            <div class="text-center col-md-12">
                <h1 class="fw-700">Inscription</h1>
                <p class="text-14px">Inscrivez-vous pour suivre le cours</p>
            </div>
        </div>
        <div class="row">
            <div class="text-center col-lg-6 d-none d-lg-block">
                <img class="mt-5" width="80%" src="assets/images/system/sign_up.png" />
            </div>
            <div class="col-lg-6">
                <div class="sign-up-form">
                    <div class="form-group">
                        <div class="row justify-content-center">
                            <div class="col-3 d-none d-sm-block">
                                <hr class="w-100" />
                            </div>
                            <div class="col-auto pt-1 text-center text-13px fw-500 text-muted">
                                Continue with email
                            </div>
                            <div class="col-3 d-none d-sm-block">
                                <hr class="w-100" />
                            </div>
                        </div>
                    </div>
                    <form action="" method="post" id="sign_up">
                        <div class="form-group">
                            <label for="first_name">First name</label>
                            <div class="input-group">
                                <span class="bg-white input-group-text" for="first_name"><i class="fas fa-user"></i></span>
                                <input type="text" name="first_name" class="form-control" placeholder="First name" aria-label="First name" aria-describedby="First name" id="first_name" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name">Last name</label>
                            <div class="input-group">
                                <span class="bg-white input-group-text" for="last_name"><i class="fas fa-user"></i></span>
                                <input type="text" name="last_name" class="form-control" placeholder="Last name" aria-label="Last name" aria-describedby="Last name" id="last_name" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="registration-email">Email</label>
                            <div class="input-group">
                                <span class="bg-white input-group-text" for="email"><i class="fas fa-user"></i></span>
                                <input type="email" name="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="Email" id="registration-email" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="registration-password">Password</label>
                            <div class="input-group">
                                <span class="bg-white input-group-text" for="password"><i class="fas fa-user"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="Password" id="registration-password" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="mt-4 btn red radius-10 w-100">Inscription'</button>
                        </div>

                        <div class="mt-4 mb-0 text-center form-group">
                            Already have an account?
                            <a class="text-15px fw-700" href="login.html">Connexion</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection